#include "HttpServer.h"
#include <microhttpd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

namespace util
{
    static MHD_Result arg_callback(void *cls,
                                   enum MHD_ValueKind,
                                   const char *key,
                                   const char *value)
    {
        util::HttpServer::IterateArgumentsArgs* args = (util::HttpServer::IterateArgumentsArgs*)cls;
        args->m_server->on_arg_recieved(key, value == nullptr ? "" : value);

        return (MHD_Result)1;
    }

    static MHD_Result
    ahc_echo(void * cls,
             struct MHD_Connection * connection,
             const char*,
             const char* method,
             const char*,
             const char*,
             size_t* upload_data_size,
             void** ptr)
    {
        static int dummy;
        util::HttpServer::RunServerArgs* args = (util::HttpServer::RunServerArgs*)cls;
        struct MHD_Response * response;
        int ret;

        if (0 != strcmp(method, "GET"))
            return MHD_NO;
        if (&dummy != *ptr)
        {
            *ptr = &dummy;
            return MHD_YES;
        }

        *ptr = NULL;

        util::HttpServer::IterateArgumentsArgs* getArgsArgs = new util::HttpServer::IterateArgumentsArgs { args->m_server };
        MHD_get_connection_values(connection, (MHD_ValueKind)(MHD_GET_ARGUMENT_KIND), arg_callback, (void*)getArgsArgs);
        delete getArgsArgs;


        response = MHD_create_response_from_buffer (strlen(args->m_response),
                                                    (void*) args->m_response,
                                                    MHD_RESPMEM_PERSISTENT);
        ret = MHD_queue_response(connection,
                                 MHD_HTTP_OK,
                                 response);
        MHD_destroy_response(response);
        return (MHD_Result)ret;
    }

    HttpServer::HttpServer()
            : m_run_server_args(nullptr)
              , m_mhd_daemon(nullptr)
              , m_on_argument_callback(nullptr)
    {

    }

    void HttpServer::run(int port)
    {
        m_run_server_args = new RunServerArgs { "", this};
        m_mhd_daemon = MHD_start_daemon(MHD_USE_THREAD_PER_CONNECTION,
                                        port,
                                        NULL,
                                        NULL,
                                        &ahc_echo,
                                        m_run_server_args,
                                        MHD_OPTION_END);


    }

    void HttpServer::stop()
    {
        MHD_stop_daemon(m_mhd_daemon);

        delete m_run_server_args;
        m_run_server_args = nullptr;
    }

    void HttpServer::set_on_arg_callback(std::function<void(std::string, std::string)> cb)
    {
        m_on_argument_callback = cb;
    }

    void HttpServer::on_arg_recieved(std::string key, std::string value)
    {
        if (m_on_argument_callback)
        {
            m_on_argument_callback(key, value);
        }
    }
}
