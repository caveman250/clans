#pragma once

#include <libsecret/secret.h>

namespace util
{
    class PasswordManager
    {
    public:
        static std::string load_token();
        static void save_token(const std::string& token);

        static bool ensure_collection_unlocked();
    };
}
