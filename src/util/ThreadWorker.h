#pragma once

namespace util
{
    class ThreadWorker
    {
    public:
        ThreadWorker();

        void run(const std::function<void(ThreadWorker*)>& task, const std::function<void(bool, util::ThreadWorker*)>& on_finished);
        void cancel();
        bool is_cancelled() const;

    private:
        mutable std::mutex m_mutex;

        bool m_cancelled;
        bool m_finished;
    };
}