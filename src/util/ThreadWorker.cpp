#include "ThreadWorker.h"

namespace util
{
    ThreadWorker::ThreadWorker()
            : m_mutex()
              , m_cancelled(false)
              , m_finished(false)
    {
    }

    void ThreadWorker::cancel()
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        m_cancelled = true;
    }

    bool ThreadWorker::is_cancelled() const
    {
        std::lock_guard<std::mutex> lock(m_mutex);
        return m_cancelled;
    }

    void ThreadWorker::run(const std::function<void(ThreadWorker*)>& task, const std::function<void(bool, util::ThreadWorker*)>& on_finished)
    {
        {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_finished = false;
            m_cancelled = false;
        }

        task(this);

        {
            std::lock_guard<std::mutex> lock(m_mutex);
            m_finished = true;
            on_finished(m_cancelled, this);
        }
    }
}