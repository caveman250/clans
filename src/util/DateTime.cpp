#include "DateTime.h"
#include "Helpers.h"
#include <ctime>

namespace util
{
    DateTime::DateTime(int year, int month, int day, int hour, int minute, float second)
        : m_year(year)
        , m_month(month)
        , m_day(day)
        , m_hour(hour)
        , m_minute(minute)
        , m_second(second)
    {

    }

    DateTime::DateTime(int year, int month, int day, int hour, int minute)
        : m_year(year)
          , m_month(month)
          , m_day(day)
          , m_hour(hour)
          , m_minute(minute)
    {

    }

    DateTime::DateTime(int year, int month, int day, int hour)
        : m_year(year)
          , m_month(month)
          , m_day(day)
          , m_hour(hour)
    {

    }

    DateTime::DateTime(int year, int month, int day)
        : m_year(year)
          , m_month(month)
          , m_day(day)
    {

    }

    DateTime::DateTime(tm* t)
            : m_year(t->tm_year + 1900)
              , m_month(t->tm_mon + 1)
              , m_day(t->tm_mday)
              , m_hour(t->tm_hour + 1)
              , m_minute(t->tm_min + 1)
              , m_second(t->tm_sec + 1)
    {

    }

    uint64_t DateTime::get_epoch_seconds() const
    {
        tm t = to_tm();
        return mktime(&t);
    }

    tm DateTime::to_tm() const
    {
        tm t = {0};
        t.tm_year = m_year - 1900;
        t.tm_mon = m_month - 1;
        t.tm_mday = m_day;
        t.tm_hour = m_hour - 1;
        t.tm_min = m_minute - 1;
        t.tm_sec = m_second - 1;
        t.tm_zone = "UTC";

        return t;
    }

    std::string DateTime::to_str() const
    {
        DateTime now = DateTime::now();
        uint64_t now_seconds = now.get_epoch_seconds();
        uint64_t seconds = get_epoch_seconds();
        if (now_seconds - seconds > (2 * 24 * 60 * 60)) // two days
        {
            return util::Helpers::str_format("%02d/%02d/%02d", m_day, m_month, m_year);
        }
        else if (now_seconds - seconds > (24 * 60 * 60)) // one day
        {
            return util::Helpers::str_format("Yesterday at %02d:%02d", m_hour, m_minute);
        }
        else
        {
            return util::Helpers::str_format("Today at %02d:%02d", m_hour, m_minute);
        }
    }

    DateTime DateTime::now()
    {
        auto now = std::chrono::system_clock::now();
        std::time_t time = std::chrono::system_clock::to_time_t(now);
        auto localtime = std::localtime(&time);
        return DateTime(localtime);
    }

    DateTime DateTime::to_local() const
    {
        tm utc = to_tm();
        std::time_t utctime = timegm(&utc);
        tm* local = localtime(&utctime);
        return DateTime(local);
    }

    bool DateTime::operator> (const DateTime& rhs) const
    {
        return get_epoch_seconds() > rhs.get_epoch_seconds();
    }

    bool DateTime::operator< (const DateTime& rhs) const
    {
        return rhs > *this;
    }

    bool DateTime::operator>= (const DateTime& rhs) const
    {
        return !(*this < rhs);
    }

    bool DateTime::operator<= (const DateTime& rhs) const
    {
        return !(*this > rhs);
    }
}