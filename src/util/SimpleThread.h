#include "ThreadWorker.h"

namespace util
{
    class ThreadWorker;
    class SimpleThread
    {
    public:
        explicit SimpleThread(const std::function<void*(ThreadWorker*)>& task, std::function<void(void*, bool, SimpleThread*)> on_finished = nullptr);
        void cancel();
    private:
        std::thread* m_thread;
        ThreadWorker* m_worker;
        std::function<void(void*, bool, util::SimpleThread*)> m_on_finished;
        Glib::Dispatcher m_main_thread_dispatcher;
        void* m_user_data = nullptr;
    };
}
