#pragma once
#include <tuple>
#include "user.h"
#include "attachment.h"
#include "embed.h"
#include "permissions.h"
#include "webhook.h"
#include "discord_object_interface.h"
#include "snowflake.h"
#include "channel.h"

// <--- means to add later

namespace SleepyDiscord {
	
	struct Reaction : public DiscordObject {
	public:
		Reaction() = default;
		~Reaction();
		//Reaction(const std::string * rawJson);
		Reaction(const json::Value & rawJSON);
		Reaction(const nonstd::string_view & json);
		//Reaction(const json::Values values);
		int count = 0;
		bool me = false;
		Emoji emoji;

		//const static std::initializer_list<const char*const> fields;
		JSONStructStart
			std::make_tuple(
				json::pair(&Reaction::count, "count", json::REQUIRIED_FIELD),
				json::pair(&Reaction::me   , "me"   , json::REQUIRIED_FIELD),
				json::pair(&Reaction::emoji, "emoji", json::REQUIRIED_FIELD)
			);
		JSONStructEnd
	};

    struct MessageReference
    {
        MessageReference() = default;
        ~MessageReference() = default;
        MessageReference(const json::Value & rawJSON);
        MessageReference(const nonstd::string_view & json);
        Snowflake<Message> message_id;
        Snowflake<Channel> channel_id;
        Snowflake<Server> guild_id;
        bool fail_if_not_exists;

        //const static std::initializer_list<const char*const> fields;
        JSONStructStart
                    std::make_tuple(
                            json::pair(&MessageReference::message_id, "message_id", json::REQUIRIED_FIELD),
                            json::pair(&MessageReference::channel_id   , "channel_id"   , json::REQUIRIED_FIELD),
                            json::pair(&MessageReference::guild_id, "guild_id", json::REQUIRIED_FIELD),
                            json::pair(&MessageReference::fail_if_not_exists, "fail_if_not_exists", json::OPTIONAL_FIELD)
                    );
        JSONStructEnd
    };

	//forward declearion
	class BaseDiscordClient;
	struct Server;

    struct ReferencedMessage : public IdentifiableDiscordObject<ReferencedMessage> {
    public:
        ReferencedMessage() = default;
        ~ReferencedMessage() = default;
        //Message(const json::Values values);
        //Message(const std::string * rawJson);
        ReferencedMessage(const json::Value& json);
        ReferencedMessage(const nonstd::string_view& json);
        //using DiscordObject::DiscordObject;

        Snowflake<Channel> channelID;
        Snowflake<Server> serverID;
        User author;
        ServerMember member;
        std::string content;
        std::string timestamp;
        std::string editedTimestamp;
        bool tts = false;
        bool mentionEveryone = false;
        std::vector<User> mentions;
        std::vector<Snowflake<User>> mentionRoles;
        std::vector<Attachment> attachments;
        std::vector<Embed> embeds;
        std::vector<Reaction> reactions;
        bool pinned = false;
        Snowflake<Webhook> webhookID;
        enum MessageType {
            DEFAULT                = 0,
            RECIPIENT_ADD          = 1,
            RECIPIENT_REMOVE       = 2,
            CALL                   = 3,
            CHANNEL_NAME_CHANGE    = 4,
            CHANNEL_ICON_CHANGE    = 5,
            CHANNEL_PINNED_MESSAGE = 6,
            GUILD_MEMBER_JOIN      = 7
        } type = DEFAULT;
        MessageReference messageReference;

        //const static std::initializer_list<const char*const> fields;
        JSONStructStart
                    std::make_tuple(
                            json::pair                           (&ReferencedMessage::ID               , "id"                 , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::channelID        , "channel_id"         , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::serverID         , "guild_id"           , json::OPTIONAL_FIELD         ),
                            json::pair                           (&ReferencedMessage::author           , "author"             , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::content          , "content"            , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::member           , "member"             , json::OPTIONAL_FIELD         ),
                            json::pair                           (&ReferencedMessage::timestamp        , "timestamp"          , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::editedTimestamp  , "edited_timestamp"   , json::NULLABLE_FIELD         ),
                            json::pair                           (&ReferencedMessage::tts              , "tts"                , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::mentionEveryone  , "mention_everyone"   , json::REQUIRIED_FIELD        ),
                            json::pair<json::ContainerTypeHelper>(&ReferencedMessage::mentions         , "mentions"           , json::REQUIRIED_FIELD        ),
                            json::pair<json::ContainerTypeHelper>(&ReferencedMessage::mentionRoles     , "mention_roles"      , json::REQUIRIED_FIELD        ),
                            json::pair<json::ContainerTypeHelper>(&ReferencedMessage::attachments      , "attachments"        , json::REQUIRIED_FIELD        ),
                            json::pair<json::ContainerTypeHelper>(&ReferencedMessage::embeds           , "embeds"             , json::REQUIRIED_FIELD        ),
                            json::pair<json::ContainerTypeHelper>(&ReferencedMessage::reactions        , "reactions"          , json::OPTIONAL_FIELD         ),
                            json::pair                           (&ReferencedMessage::pinned           , "pinned"             , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::webhookID        , "webhook_id"         , json::OPTIONAL_FIELD         ),
                            json::pair<json::EnumTypeHelper     >(&ReferencedMessage::type             , "type"               , json::REQUIRIED_FIELD        ),
                            json::pair                           (&ReferencedMessage::messageReference , "message_reference"  , json::OPTIONAL_FIELD        )
                    );
        JSONStructEnd
    };

	struct Message : public IdentifiableDiscordObject<Message> {
	public:
		Message() = default;
		~Message() = default;
		//Message(const json::Values values);
		//Message(const std::string * rawJson);
		Message(const json::Value& json);
		Message(const nonstd::string_view& json);
		//using DiscordObject::DiscordObject;
		bool startsWith(const std::string& test);
		std::size_t length();
		bool isMentioned(Snowflake<User> ID);
		bool isMentioned(User& _user);
		Message send(BaseDiscordClient * client);
		Message reply(BaseDiscordClient * client, std::string message,
			Embed embed = Embed()
		);

		Snowflake<Channel> channelID;
		Snowflake<Server> serverID;
		User author;
		ServerMember member;
		std::string content;
		std::string timestamp;
		std::string editedTimestamp;
		bool tts = false;
		bool mentionEveryone = false;
		std::vector<User> mentions;
		std::vector<Snowflake<User>> mentionRoles;
		std::vector<Attachment> attachments;
		std::vector<Embed> embeds;
		std::vector<Reaction> reactions;
		bool pinned = false;
		Snowflake<Webhook> webhookID;
		enum MessageType {
			DEFAULT                = 0,
			RECIPIENT_ADD          = 1,
			RECIPIENT_REMOVE       = 2,
			CALL                   = 3,
			CHANNEL_NAME_CHANGE    = 4,
			CHANNEL_ICON_CHANGE    = 5,
			CHANNEL_PINNED_MESSAGE = 6,
			GUILD_MEMBER_JOIN      = 7
		} type = DEFAULT;
        MessageReference messageReference;
        ReferencedMessage referencedMessage;

		//const static std::initializer_list<const char*const> fields;
		JSONStructStart
			std::make_tuple(
				json::pair                           (&Message::ID               , "id"                 , json::REQUIRIED_FIELD        ),
				json::pair                           (&Message::channelID        , "channel_id"         , json::REQUIRIED_FIELD        ),
				json::pair                           (&Message::serverID         , "guild_id"           , json::OPTIONAL_FIELD         ),
				json::pair                           (&Message::author           , "author"             , json::REQUIRIED_FIELD        ),
				json::pair                           (&Message::content          , "content"            , json::REQUIRIED_FIELD        ),
				json::pair                           (&Message::member           , "member"             , json::OPTIONAL_FIELD         ),
				json::pair                           (&Message::timestamp        , "timestamp"          , json::REQUIRIED_FIELD        ),
				json::pair                           (&Message::editedTimestamp  , "edited_timestamp"   , json::NULLABLE_FIELD         ),
				json::pair                           (&Message::tts              , "tts"                , json::REQUIRIED_FIELD        ),
				json::pair                           (&Message::mentionEveryone  , "mention_everyone"   , json::REQUIRIED_FIELD        ),
				json::pair<json::ContainerTypeHelper>(&Message::mentions         , "mentions"           , json::REQUIRIED_FIELD        ),
				json::pair<json::ContainerTypeHelper>(&Message::mentionRoles     , "mention_roles"      , json::REQUIRIED_FIELD        ),
				json::pair<json::ContainerTypeHelper>(&Message::attachments      , "attachments"        , json::REQUIRIED_FIELD        ),
				json::pair<json::ContainerTypeHelper>(&Message::embeds           , "embeds"             , json::REQUIRIED_FIELD        ),
				json::pair<json::ContainerTypeHelper>(&Message::reactions        , "reactions"          , json::OPTIONAL_FIELD         ),
				json::pair                           (&Message::pinned           , "pinned"             , json::REQUIRIED_FIELD        ),
				json::pair                           (&Message::webhookID        , "webhook_id"         , json::OPTIONAL_FIELD         ),
				json::pair<json::EnumTypeHelper     >(&Message::type             , "type"               , json::REQUIRIED_FIELD        ),
                json::pair                           (&Message::messageReference , "message_reference"  , json::OPTIONAL_FIELD        ),
                json::pair                           (&Message::referencedMessage             , "referenced_message", json::NULLABLE_FIELD)
			);
		JSONStructEnd
	};

	struct MessageRevisions {
		MessageRevisions(const json::Value& json) :
			messageID(json["id"]), channelID(json["channel_id"]), RevisionsJSON(json)
		{}
		inline void applyChanges(Message& outOfDateMessage) {
			assert(outOfDateMessage.ID == messageID);
			json::fromJSON(outOfDateMessage, RevisionsJSON);
		}
		Snowflake<Message> messageID;
		Snowflake<Channel> channelID;
		const json::Value& RevisionsJSON;
	};

	struct SendMessageParams : public DiscordObject {
	public:
		Snowflake<Channel> channelID;
		std::string content = {};
		bool tts = false;
		Embed embed = Embed::Flag::INVALID_EMBED;
		JSONStructStart
			std::make_tuple(
				json::pair(&SendMessageParams::content, "content", json::REQUIRIED_FIELD),
				json::pair(&SendMessageParams::tts    , "tts"    , json::OPTIONAL_FIELD ),
				json::pair(&SendMessageParams::embed  , "embed"  , json::OPTIONAL_FIELD )
			);
		JSONStructEnd
	};
}