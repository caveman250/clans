#include "AttachmentArea.h"
#include "AttachmentWidget.h"
#include "Application.h"
#include "MessageBar.h"

namespace ui
{

    AttachmentArea::AttachmentArea(MessageBar* parent)
    {
        m_child = new Gtk::Revealer();
        m_child->set_parent(*this);

        m_box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        auto separator = new Gtk::Separator();
        m_box->append(*separator);
        m_child->set_child(*m_box);

        auto scrolled_window = new Gtk::ScrolledWindow();
        m_box->append(*scrolled_window);
        scrolled_window->set_policy(Gtk::PolicyType::AUTOMATIC, Gtk::PolicyType::NEVER);

        auto viewport = new Gtk::Viewport(Gtk::Adjustment::create(0, 0, 1), Gtk::Adjustment::create(0, 0, 1));
        scrolled_window->set_child(*viewport);

        m_attachment_box = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 10);
        m_attachment_box->set_margin(5);
        viewport->set_child(*m_attachment_box);

        AttachmentWidget* attachmentWidget = new AttachmentWidget(this);
        attachmentWidget->set_valign(Gtk::Align::START);
        m_attachment_box->append(*attachmentWidget);

        parent->signal_send().connect([this](const std::string& , std::vector<std::string>, const SleepyDiscord::Message&)
        {
            set_reveal_child(false);
            for (const auto& child : m_children)
            {
                m_attachment_box->remove(*child);
            }
            m_children.clear();
        });
    }

    void AttachmentArea::set_reveal_child(bool reveal)
    {
        m_child->set_reveal_child(reveal);
    }

    bool AttachmentArea::get_reveal_child()
    {
        return m_child->get_reveal_child();
    }

    void AttachmentArea::add_attachment(const std::string file_path)
    {
        AttachmentWidget* attachmentWidget = new AttachmentWidget(this, file_path);
        attachmentWidget->set_valign(Gtk::Align::START);
        m_attachment_box->append(*attachmentWidget);

        m_children.push_back(attachmentWidget);
    }

    void AttachmentArea::remove_attachment(AttachmentWidget* attachment)
    {
        m_attachment_box->remove(*attachment);
        m_children.erase(std::remove(m_children.begin(), m_children.end(), attachment), m_children.end());
    }

    void AttachmentArea::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void AttachmentArea::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            m_child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void AttachmentArea::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    std::vector<std::string> AttachmentArea::get_attachments()
    {
        std::vector<std::string> ret;
        for (const auto& child : m_children)
        {
            if (!child->get_filepath().empty())
            {
                ret.push_back(child->get_filepath());
            }
        }

        return ret;
    }
}