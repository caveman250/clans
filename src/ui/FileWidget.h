#pragma once

namespace ui
{
    class FileWidget : public Gtk::Widget
    {
    public:
        FileWidget(const SleepyDiscord::Attachment& file);
        virtual ~FileWidget();

    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr <Gtk::Snapshot>& snapshot) override;
        Gtk::SizeRequestMode get_request_mode_vfunc() const override;

        Gtk::Box* m_child;
    };
}

