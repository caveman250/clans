#pragma once

namespace ui
{
    class Popup
    {
    public:
        Popup(Gtk::Widget* parent, int override_x = -1, int override_y = -1);

        void open();
        void close(bool snap);

        void on_flap_fold_state_changed();
        sigc::signal<void()> signal_closed() { return m_closed_signal; }
    protected:
        Gtk::Widget* m_parent = nullptr;
        int m_override_x = -1;
        int m_override_y = -1;
        Gtk::Popover* m_popover = nullptr;
        Gtk::Box* m_box = nullptr;
    private:
        void create_box();
        void create_popover(Gtk::Widget* parent);

        guint m_flap_folded_signal_handler;
        sigc::signal<void()> m_closed_signal;
    };
}
