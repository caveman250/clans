#pragma once

#include "Page.h"

namespace ui
{
    class ImageViewer : public Page
    {
    public:
        ImageViewer();
        void set_texture(const Glib::RefPtr<Gdk::Texture>& texture);

        void create_header_bar() override;
    private:
        void calculate_image_bounds();

        virtual void size_allocate_vfunc(int width, int height, int baseline) override;
        virtual void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        virtual void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        Gtk::Box* m_child = nullptr;
        Gtk::Box* m_image_box = nullptr;
        Glib::RefPtr<Gdk::Texture> m_texture = nullptr;
        float m_aspect = 1.f;
        float m_offset_x = 0.f;
        float m_offset_y = 0.f;
        float m_drag_offset_x = 0.f;
        float m_drag_offset_y = 0.f;

        float m_pre_zoom_scale = 1.f;
        float m_scale = 1.f;

        double m_mouse_x = 0.f;
        double m_mouse_y = 0.f;

        graphene_point_t m_image_min { 0, 0 };
        graphene_point_t m_image_max  { 0, 0 };
    };
}