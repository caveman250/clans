#include "EmptyMessageView.h"
#include "MessageWidget.h"
#include "../Application.h"
#include "../discord/DiscordClient.h"
#include "../util/SimpleThread.h"
#include "GifWidget.h"
#include "../util/HtmlMetaParser.h"
#include "MessageBar.h"

namespace ui
{
    EmptyMessageView::EmptyMessageView()
            : Page()
    {
        set_vexpand(true);

        m_child = new Gtk::Box(Gtk::Orientation::VERTICAL, 10);
        m_child->set_parent(*this);
        m_child->set_valign(Gtk::Align::FILL);

        auto image = new Gtk::Image();
        image->set_from_icon_name("user-available-symbolic");
        image->set_icon_size(Gtk::IconSize::INHERIT);
        image->set_pixel_size(128);
        image->set_vexpand(true);
        image->set_valign(Gtk::Align::END);
        m_child->append(*image);

        auto title = new Gtk::Label("No channel selected");
        title->add_css_class("empty-channel-title");
        m_child->append(*title);

        auto subtext = new Gtk::Label("Select a channel to start messaging");
        subtext->set_vexpand(true);
        subtext->set_valign(Gtk::Align::START);
        m_child->append(*subtext);
    }

    EmptyMessageView::~EmptyMessageView()
    {
        m_child->unparent();
        delete m_child;
    }

    void EmptyMessageView::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void EmptyMessageView::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void EmptyMessageView::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    void EmptyMessageView::create_header_bar()
    {
        m_header_bar = (GtkHeaderBar*)adw_header_bar_new();
        adw_header_bar_set_show_start_title_buttons((AdwHeaderBar*)m_header_bar, false);
        adw_header_bar_set_title_widget((AdwHeaderBar*)m_header_bar, (GtkWidget*)gtk_box_new(GTK_ORIENTATION_HORIZONTAL, 0));
        gtk_box_prepend(m_child->gobj(), (GtkWidget*)m_header_bar);

        add_flap_button(false);
    }
}