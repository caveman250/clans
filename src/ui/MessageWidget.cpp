#include "MessageWidget.h"
#include "util/Helpers.h"
#include "util/ImageDownloader.h"
#include "util/HtmlMetaParser.h"
#include <regex>
#include <filesystem>
#include <Application.h>
#include "GifWidget.h"
#include "ReactionWidget.h"
#include "util/SimpleThread.h"
#include "discord/DiscordClient.h"
#include "ui/FileWidget.h"
#include "util/ImageCache.h"
#include "util/CancellationToken.h"
#include "ImageViewer.h"
#include "EmojiPopup.h"
#include "MessageContextMenuPopup.h"
#include "ReplyWidget.h"

namespace ui
{
    MessageWidget::MessageWidget()
            : Gtk::Widget()
    {

    }

    void MessageWidget::init(const Glib::RefPtr<discord::MessageWidgetData>& message_data)
    {
        if (m_child)
        {
            reset();
        }

        m_message = message_data->get_message();
        if (message_data->get_loading_placeholder())
        {
            m_child = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 0);
            m_child->set_parent(*this);

            auto spinner = new Gtk::Spinner();
            spinner->set_size_request(64, 64);
            spinner->set_halign(Gtk::Align::CENTER);
            spinner->set_hexpand(true);
            spinner->start();
            m_child->append(*spinner);
            return;
        }

        setup(message_data->get_show_name(), message_data->get_hide_text());

        if (!message_data->get_display_attachment().empty())
        {
            setup_attachment(message_data->get_display_attachment());
        }
        else if (!message_data->property_image_url().get_value().empty())
        {
            setup_image_url(message_data);
        }

        message_data->property_image_url().signal_changed().connect([this, message_data]()
        {
            setup_image_url(message_data);
        });

        setup_reactions();
    }

    void MessageWidget::setup_image_url(const Glib::RefPtr<discord::MessageWidgetData>& message)
    {
        if (m_gif)
        {
            m_gif->unparent();
            delete m_gif;
        }

        if (m_picture)
        {
            m_picture->unparent();
            delete m_picture;
        }

        const std::string& image_url = message->property_image_url().get_value();
        if (image_url.find(".gif") != std::string::npos ||
            image_url.find(".GIF") != std::string::npos)
        {
            setup_gif(image_url);
        }
        else if (image_url.find(".jpg") != std::string::npos ||
                 image_url.find(".JPG") != std::string::npos ||
                 image_url.find(".jpeg") != std::string::npos ||
                 image_url.find(".JPEG") != std::string::npos ||
                 image_url.find(".png") != std::string::npos ||
                 image_url.find(".PNG") != std::string::npos)
        {
            setup_picture(image_url, 1, -1, -1);
        }
    }

    void MessageWidget::reset()
    {
        m_child->unparent();
        delete m_child;

        if (m_image_cancellation_token)
        {
            m_image_cancellation_token->cancel();
            m_image_cancellation_token = nullptr;
        }
        if (m_avatar_cancellation_token)
        {
            m_avatar_cancellation_token->cancel();
            m_avatar_cancellation_token = nullptr;
        }

        if (m_gif)
        {
            delete m_gif;
        }

        if (m_picture)
        {
            delete m_picture;
        }
    }

    MessageWidget::~MessageWidget()
    {
        reset();
    }

    void MessageWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void MessageWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            m_child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void MessageWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    Gtk::SizeRequestMode MessageWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::HEIGHT_FOR_WIDTH;
    }

    void MessageWidget::setup(bool show_name, bool hide_text)
    {
        add_css_class("message-widget");

        set_margin_start(5);
        set_margin_end(5);

        m_child = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 0);
        m_vertical_box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        m_child->append(*m_vertical_box);

        m_child->set_parent(*this);

        m_content_box = new Gtk::Box(Gtk::Orientation::VERTICAL, 0);
        m_content_box->set_hexpand(true);
        auto gesture_long_press = Gtk::GestureLongPress::create();
        gesture_long_press->signal_begin().connect([this](Gdk::EventSequence*)
                                                   {
                                                       auto flap = Application::Get()->get_flap();
                                                       if (!adw_flap_get_folded(flap))
                                                       {
                                                           return;
                                                       }
                                                       add_css_class("message-focus");
                                                   });
        gesture_long_press->signal_cancelled().connect([this]()
                                                       {
                                                           if (has_css_class("message-focus"))
                                                           {
                                                               remove_css_class("message-focus");
                                                           }
                                                       });
        gesture_long_press->signal_cancel().connect([this](Gdk::EventSequence*)
                                                    {
                                                        if (has_css_class("message-focus"))
                                                        {
                                                            remove_css_class("message-focus");
                                                        }
                                                    });
        gesture_long_press->signal_pressed().connect([this](double x, double y)
                                                     {
                                                         if (has_css_class("message-focus"))
                                                         {
                                                             remove_css_class("message-focus");
                                                         }

                                                         auto flap = Application::Get()->get_flap();
                                                         if (!adw_flap_get_folded(flap))
                                                         {
                                                             return;
                                                         }

                                                         Gtk::Allocation alloc = m_content_box->get_allocation();

                                                         new MessageContextMenuPopup(m_content_box, this, x - alloc.get_x(), y - alloc.get_y());
                                                     });
        m_content_box->add_controller(gesture_long_press);

        auto gesture_click = Gtk::GestureClick::create();
        gesture_click->set_button(3);
        gesture_click->signal_released().connect([this](int n_press, double x, double y)
                                                 {
                                                     new MessageContextMenuPopup(m_content_box, this, x, y);
                                                 });
        m_content_box->add_controller(gesture_click);

        if (!m_message.messageReference.message_id.empty())
        {
            ReplyWidget* reply = new ReplyWidget(m_message.referencedMessage);
            reply->set_margin_bottom(10);
            m_vertical_box->append(*reply);
        }

        auto horizontal_box = new Gtk::Box(Gtk::Orientation::HORIZONTAL);
        horizontal_box->append(*m_content_box);

        if (show_name || !m_message.messageReference.message_id.empty())
        {
            GtkWidget* avatar = adw_avatar_new(32, m_message.author.username.c_str(), true);
            gtk_widget_set_valign(avatar, GTK_ALIGN_START);
            gtk_widget_set_margin_start(avatar, 5);
            gtk_widget_set_margin_end(avatar, 10);
            gtk_box_prepend(horizontal_box->gobj(), avatar);

            set_margin_top(10);

            auto title_box = new Gtk::Box(Gtk::Orientation::HORIZONTAL);
            title_box->set_vexpand(true);
            m_content_box->append(*title_box);

            auto name_label = new Gtk::Label();
            name_label->set_text(m_message.author.username);
            name_label->set_xalign(0);
            name_label->set_margin_end(5);
            name_label->set_margin_bottom(5);
            name_label->add_css_class("message_title");
            title_box->append(*name_label);

            auto date_label = new Gtk::Label();
            util::DateTime date_time = util::Helpers::discord_time_str_to_datetime(m_message.timestamp);
            util::DateTime local_time = date_time.to_local();
            date_label->set_text(local_time.to_str());
            date_label->set_opacity(0.5);
            date_label->set_yalign(0);
            date_label->set_halign(Gtk::Align::END);
            date_label->set_margin_end(5);
            date_label->set_margin_bottom(5);
            date_label->add_css_class("message-timestamp");
            title_box->append(*date_label);

            if (!m_message.author.avatar.empty())
            {
                std::string avatar_url = util::Helpers::str_format("https://cdn.discordapp.com/avatars/%s/%s.png?size=64", m_message.author.ID.string().c_str(), m_message.author.avatar.c_str());
                m_avatar_cancellation_token = new util::CancellationToken();
                util::ImageCache::get()->get_image(avatar_url, [this, avatar](const std::string& file_path)
                {
                    try
                    {
                        auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
                        adw_avatar_set_custom_image((AdwAvatar*) avatar, (GdkPaintable*) texture->gobj());
                    }
                    catch (std::exception& e)
                    {
                        std::cout << e.what() << std::endl;
                    }

                    m_avatar_cancellation_token = nullptr;
                }, m_avatar_cancellation_token);
            }
        }
        else
        {
            m_content_box->set_margin_start(47); // 32 + margin start (5) + margin end (10)
        }

        m_vertical_box->append(*horizontal_box);

        if (!m_message.content.empty() && !hide_text)
        {
            const char* escaped_text = g_markup_escape_text(m_message.content.c_str(), (gssize)m_message.content.length());
            std::vector<std::string> links = util::Helpers::extract_urls_from_str(escaped_text);
            std::string message_content = escaped_text;

            for (const auto& link : links)
            {
                std::string markup_link = util::Helpers::str_format("<a href=\"%s\">%s</a>", link.c_str(), link.c_str());
                message_content.replace(message_content.find(link), link.length(), markup_link);
            }

            auto label = new Gtk::Label();
            for (const auto& mention : m_message.mentions)
            {
                std::string search_string = util::Helpers::str_format("&lt;@!%s&gt;", mention.ID.string().c_str());
                auto it = message_content.find(search_string);

                if (it == std::string::npos)
                {
                    std::string search_string = util::Helpers::str_format("&lt;@%s&gt;", mention.ID.string().c_str());
                    it = message_content.find(search_string);
                    if (it == std::string::npos)
                    {
                        continue;
                    }
                }

                std::string colour = adw_style_manager_get_dark(adw_style_manager_get_default()) ? "darkblue" : "lightblue";
                std::string replacement = util::Helpers::str_format(R"( <span background="%s">@%s</span> )", colour.c_str(), mention.username.c_str());
                message_content = message_content.replace(it, search_string.size(), replacement);
            }

            label->set_use_markup(true);
            label->set_markup(message_content);
            label->set_wrap_mode(Pango::WrapMode::WORD_CHAR);
            label->set_wrap(true);
            label->set_margin_top(5);
            label->set_margin_bottom(5);
            label->set_margin_end(5);
            label->set_xalign(0.f);
            label->add_css_class("message_text");
            label->signal_activate_link().connect([](const Glib::ustring& url)
            {
                util::Helpers::handle_link(url);
                return true;
            }, false);

            m_content_box->append(*label);
        }
    }

    void MessageWidget::setup_attachment(const SleepyDiscord::Attachment& attachment)
    {
        if (attachment.url.find(".gif") != std::string::npos ||
                attachment.url.find(".GIF") != std::string::npos)
        {
            setup_gif(attachment.url);
        }
        else if (attachment.url.find(".jpg") != std::string::npos ||
                 attachment.url.find(".JPG") != std::string::npos ||
                 attachment.url.find(".jpeg") != std::string::npos ||
                 attachment.url.find(".JPEG") != std::string::npos ||
                 attachment.url.find(".png") != std::string::npos ||
                 attachment.url.find(".PNG") != std::string::npos)
        {
            setup_picture(attachment.url, (float)attachment.height / (float)attachment.width, attachment.width, attachment.height);
        }
        else
        {
            auto file_widget = new ui::FileWidget(attachment);
            m_content_box->append(*file_widget);
        }
    }

    void MessageWidget::setup_gif(const std::string& url)
    {
        m_gif = new ui::GifWidget(this);
        m_gif->set_margin_end(5);
        m_gif->set_margin_top(5);
        m_gif->set_margin_bottom(5);
        m_gif->set_halign(Gtk::Align::START);
        m_gif->get_picture()->set_halign(Gtk::Align::START);

        m_content_box->append(*m_gif);

        m_image_cancellation_token = new util::CancellationToken();
        util::ImageCache::get()->get_image(url, [this](const std::string& file_path)
        {
            m_gif->set_gif(file_path);
            m_image_cancellation_token = nullptr;
        }, m_image_cancellation_token);
    }

    void MessageWidget::setup_picture(const std::string& url, float aspect_ratio, int width, int height)
    {
        m_picture = new Gtk::Picture();
        m_picture->set_size_request(-1, 300.f * aspect_ratio);
        m_picture->set_margin_end(5);
        m_picture->set_margin_top(5);
        m_picture->set_margin_bottom(5);
        m_picture->set_halign(Gtk::Align::START);

        auto gesture_click = Gtk::GestureClick::create();
        gesture_click->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press <= 0)
            {
                return;
            }

            ImageViewer* image_viewer = new ImageViewer();
            image_viewer->create_header_bar();
            Application::Get()->add_page(image_viewer);
            image_viewer->set_texture(std::static_pointer_cast<Gdk::Texture>(m_picture->get_paintable()));
        });
        m_picture->add_controller(gesture_click);

        m_content_box->append(*m_picture);

        m_image_cancellation_token = new util::CancellationToken();
        util::ImageCache::get()->get_image(url, [this, width, height](const std::string& file_path)
        {
            try
            {
                auto texture = Gdk::Pixbuf::create_from_file(file_path);

                // gdk ignores rotation of photos taken on android for some reason.
                // this fixes all the cases i have identified but i doubt its comprehensive.
                int texture_width = texture->get_width();
                int texture_height = texture->get_height();
                if (texture_width > 0 && texture_height > 0 && texture_width != width && texture_height != height)
                {
                    texture = texture->rotate_simple(Gdk::Pixbuf::Rotation::CLOCKWISE);
                }
                m_picture->set_pixbuf(texture);
            }
            catch (std::exception& e)
            {
                std::cout << e.what() << std::endl;
            }
            m_image_cancellation_token = nullptr;
        }, m_image_cancellation_token);
    }

    void MessageWidget::setup_reactions()
    {
        m_reaction_box = new Gtk::FlowBox();
        m_reaction_box->set_homogeneous(false);
        m_reaction_box->set_orientation(Gtk::Orientation::HORIZONTAL);
        m_reaction_box->set_margin_end(5);
        m_reaction_box->set_max_children_per_line(500);
        m_reaction_box->set_margin_start(42);
        m_reaction_box->set_selection_mode(Gtk::SelectionMode::NONE);
        m_vertical_box->append(*m_reaction_box);

        if (!m_message.reactions.empty())
        {
            for (const auto& reaction : m_message.reactions)
            {
                add_reaction(reaction);
            }
        }

        m_add_button = new Gtk::Button();
        m_add_button->set_icon_name("list-add-symbolic");
        m_add_button->add_css_class("flat");
        m_add_button->signal_clicked().connect([this]()
        {
            new EmojiPopup(m_add_button, [this](const std::string& emoji)
                           {
                               Application::Get()->get_discord_client()->addReaction(m_message.channelID, m_message.ID, emoji);
                           },
                           [this](const SleepyDiscord::Emoji& emoji)
                           {
                               std::string url = util::Helpers::str_format("%s:%s", emoji.name.c_str(), emoji.ID.string().c_str());
                               Application::Get()->get_discord_client()->addReaction(m_message.channelID, m_message.ID, url);
                           });
        });
        m_reaction_box->insert(*m_add_button, -1);
        m_add_button->set_visible(m_reaction_widgets.size() > 0);
    }

    void MessageWidget::add_reaction(const SleepyDiscord::Reaction& reaction)
    {
        std::string reaction_text = reaction.emoji.requireColons ? util::Helpers::str_format(":%s:", reaction.emoji.name.c_str()) : reaction.emoji.name;
        auto reaction_widget = new ui::ReactionWidget(reaction, m_message.channelID, m_message);
        reaction_widget->set_halign(Gtk::Align::START);
        reaction_widget->set_tooltip_text("...");
        m_reaction_box->insert(*reaction_widget, -1);
        m_reaction_widgets.push_back(reaction_widget);
    }

    void MessageWidget::add_reaction(const SleepyDiscord::Emoji& emoji, bool me)
    {
        auto it = std::find_if(m_reaction_widgets.begin(), m_reaction_widgets.end(), [emoji](const ui::ReactionWidget* reaction_widget){ return reaction_widget->get_reaction().emoji.name == emoji.name;});

        if (it != m_reaction_widgets.end())
        {
            auto reaction_widget = (*it);
            SleepyDiscord::Reaction reaction = reaction_widget->get_reaction();
            reaction.count++;
            reaction.me |= me;
            reaction_widget->set_reaction(reaction);
            reaction_widget->update_count_label();
        }
        else
        {
            SleepyDiscord::Reaction reaction;
            reaction.emoji = emoji;
            reaction.count = 1;
            reaction.me = me;

            std::string reaction_text = reaction.emoji.requireColons ? util::Helpers::str_format(":%s:", reaction.emoji.name.c_str()) : reaction.emoji.name;
            auto reaction_widget = new ui::ReactionWidget(reaction, m_message.channelID, m_message);
            reaction_widget->set_halign(Gtk::Align::START);
            reaction_widget->set_tooltip_text("...");
            m_reaction_box->insert(*reaction_widget, m_reaction_widgets.size());
            m_reaction_widgets.push_back(reaction_widget);
            m_add_button->set_visible(true);
        }
    }

    void MessageWidget::delete_reaction(const SleepyDiscord::Emoji& emoji, bool me)
    {
        auto it = std::find_if(m_reaction_widgets.begin(), m_reaction_widgets.end(), [emoji](const ui::ReactionWidget* reaction_widget){ return reaction_widget->get_reaction().emoji.name == emoji.name;});
        if (it != m_reaction_widgets.end())
        {
            auto reaction_widget = (*it);
            SleepyDiscord::Reaction reaction = reaction_widget->get_reaction();
            reaction.count--;
            if (me)
            {
                reaction.me = false;
            }
            if (reaction.count > 0)
            {
                reaction_widget->set_reaction(reaction);
                reaction_widget->update_count_label();
            }
            else
            {
                m_reaction_box->remove(*reaction_widget);
                m_reaction_widgets.erase(std::remove(m_reaction_widgets.begin(), m_reaction_widgets.end(), reaction_widget), m_reaction_widgets.end());
                delete reaction_widget;

                if (m_reaction_widgets.empty())
                {
                    m_add_button->set_visible(false);
                }
            }
        }
    }
}