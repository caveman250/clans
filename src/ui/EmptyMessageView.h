#pragma once
#include "pch.h"
#include "Page.h"

namespace ui
{
    class EmptyMessageView : public Page
    {
    public:
        EmptyMessageView();
        ~EmptyMessageView() override;

        void create_header_bar() override;

    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        Gtk::Box* m_child;
    };
}
