#include <Application.h>
#include <util/Helpers.h>
#include "MessageBar.h"
#include "AttachmentArea.h"
#include "EmojiPopup.h"

namespace ui
{
    MessageBar::MessageBar()
        : Gtk::Widget()
    {
        auto main_box = new Gtk::Box(Gtk::Orientation::VERTICAL, 5);
        auto start_separator = new Gtk::Separator();
        main_box->append(*start_separator);

        auto message_bar_box = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 5);
        main_box->append(*message_bar_box);

        m_attachment_area = new ui::AttachmentArea(this);
        main_box->append(*m_attachment_area);

        auto attach_button = new Gtk::Button();
        attach_button->set_icon_name("mail-attachment");
        attach_button->signal_clicked().connect([this]()
        {
            m_attachment_area->set_reveal_child(!m_attachment_area->get_reveal_child());
        });
        attach_button->set_valign(Gtk::Align::START);
        attach_button->set_margin_start(5);
        attach_button->set_margin_bottom(5);
        attach_button->set_margin_top(5);
        message_bar_box->append(*attach_button);

        auto box = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 0);
        box->set_margin_start(5);
        box->set_margin_end(5);
        box->set_margin_top(5);
        box->set_margin_bottom(5);
        box->add_css_class("message-input");
        message_bar_box->append(*box);

        auto scrolled_window = new Gtk::ScrolledWindow();
        scrolled_window->set_policy(Gtk::PolicyType::NEVER, Gtk::PolicyType::NEVER);
        box->append(*scrolled_window);

        m_text_view = new Gtk::TextView();
        m_text_view->set_hexpand(true);
        m_text_view->set_wrap_mode(Gtk::WrapMode::WORD);
        m_text_view->add_css_class("message-input-text");
        auto focus_controller = Gtk::EventControllerFocus::create();
        focus_controller->signal_enter().connect([box]()
        {
            box->add_css_class("message-input-focused");
        });
        focus_controller->signal_leave().connect([box]()
        {
            box->remove_css_class("message-input-focused");
        });
        m_text_view->add_controller(focus_controller);
        scrolled_window->set_child(*m_text_view);

        auto emoji_button = new Gtk::Button();
        emoji_button->set_icon_name("face-smile-symbolic");
        emoji_button->signal_clicked().connect([emoji_button]()
        {
            new EmojiPopup(emoji_button, [](const std::string& emoji)
            {
                Application::Get()->get_message_bar()->append_text(emoji);
            },
            [](const SleepyDiscord::Emoji& emoji)
            {
                Application::Get()->get_message_bar()->append_text(util::Helpers::str_format("<:%s:%s>", emoji.name.c_str(), emoji.ID.string().c_str()));
            });
        });
        emoji_button->set_valign(Gtk::Align::END);
        emoji_button->set_margin_bottom(5);
        emoji_button->set_margin_top(5);
        message_bar_box->append(*emoji_button);

        auto send_button = new Gtk::Button();
        send_button->set_icon_name("mail-send-symbolic");
        send_button->signal_clicked().connect([this]()
        {
            signal_send().emit(get_text(), m_attachment_area->get_attachments(), m_replying_to_message);
            clear_text();
            clear_reply_status();
        });
        send_button->add_css_class("suggested-action");
        send_button->set_valign(Gtk::Align::END);
        send_button->set_margin_end(5);
        send_button->set_margin_bottom(5);
        send_button->set_margin_top(5);
        message_bar_box->append(*send_button);

        main_box->set_parent(*this);
        m_child = main_box;
    }

    MessageBar::~MessageBar() = default;

    void MessageBar::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void MessageBar::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void MessageBar::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    std::string MessageBar::get_text() const
    {
        return m_text_view->get_buffer()->get_text();
    }

    void MessageBar::clear_text()
    {
        m_text_view->get_buffer()->set_text("");
    }

    void MessageBar::append_text(const std::string& text)
    {
        m_text_view->get_buffer()->insert_at_cursor(text);
    }

    void MessageBar::set_replying_to(const SleepyDiscord::Message& message)
    {
        m_replying_to_message = message;
        if (!m_replying_to_box)
        {
            m_replying_to_box = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 10);
            m_replying_to_box->add_css_class("reply-to-box");

            auto label = new Gtk::Label("Replying to");
            label->set_margin_start(5);
            m_replying_to_box->append(*label);

            m_reply_name_label = new Gtk::Label(m_replying_to_message.author.username);
            m_reply_name_label->add_css_class("bold");
            m_replying_to_box->append(*m_reply_name_label);

            auto button = new Gtk::Button();
            button->set_icon_name("edit-delete-symbolic");
            button->add_css_class("flat");
            button->set_hexpand(true);
            button->set_halign(Gtk::Align::END);
            button->signal_clicked().connect([this]()
            {
                clear_reply_status();
            });
            m_replying_to_box->append(*button);

            m_child->prepend(*m_replying_to_box);
        }
        else
        {
            m_replying_to_box->set_visible(true);
            m_reply_name_label->set_text(m_replying_to_message.author.username);
        }
    }

    void MessageBar::clear_reply_status()
    {
        m_replying_to_message = SleepyDiscord::Message();
        if (m_replying_to_box)
        {
            m_replying_to_box->set_visible(false);
        }
    }
}