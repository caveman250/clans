#pragma once

namespace util
{
    class ImageDownloader;
}

namespace ui
{
    class SideBar : public Gtk::Widget
    {
    public:
        SideBar();
        ~SideBar() override;

        void on_discord_client_ready();
        void mark_channel_unread(const SleepyDiscord::Snowflake<SleepyDiscord::Channel>& channel);
        void mark_channel_read(const SleepyDiscord::Snowflake<SleepyDiscord::Channel>& channel);

        void update_user_voice_states();
    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        AdwExpanderRow* create_dms_item();
        //AdwActionRow* create_dm_item(const SleepyDiscord& channel, AdwExpanderRow* parent);

        void create_server_item(const SleepyDiscord::Server& server, AdwExpanderRow* text_item, AdwExpanderRow* voice_item);
        AdwActionRow* create_text_channel_item(const SleepyDiscord::Channel& channel, AdwExpanderRow* parent);
        Gtk::ListBoxRow* create_voice_channel_item(const SleepyDiscord::Channel& channel, AdwExpanderRow* parent);

        Gtk::Box* m_child = nullptr;
        Gtk::ScrolledWindow* m_scrolled_window = nullptr;
        Gtk::ListBox* m_listbox = nullptr;
        Gtk::Box* m_mainbox = nullptr;
        Gtk::Spinner* m_spinner = nullptr;

        bool m_setup = false;
        std::unordered_map<int64_t, AdwActionRow*> m_channel_items;
        std::unordered_map<int64_t, Gtk::Box*> m_voice_channel_boxes;
        std::vector<SleepyDiscord::Channel*> m_channel_pointers;

        // Signal handlers
        sigc::connection m_new_message_connection;
    };
}
