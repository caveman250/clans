#include "EmojiPopup.h"
#include "EmojiWidget.h"
#include "Application.h"
#include "MessageBar.h"

namespace ui
{
    EmojiPopup::EmojiPopup(Gtk::Widget* parent, std::function<void(const std::string&)> on_selected, std::function<void(const SleepyDiscord::Emoji&)> on_server_emoji_selected, int override_x, int override_y)
        : Popup(parent, override_x, override_y)
    {
        auto emoji_widget = new ui::EmojiWidget();
        emoji_widget->set_vexpand(true);
        emoji_widget->signal_emoji_selected().connect([this, on_selected](const std::string& emoji)
        {
            on_selected(emoji);
            close(false);
        });
        emoji_widget->signal_server_emoji_selected().connect([this, on_server_emoji_selected](const SleepyDiscord::Emoji& emoji)
        {
            on_server_emoji_selected(emoji);
            close(false);
        });

        m_box->append(*emoji_widget);
        m_box->set_size_request(-1, 400);

        open();
    }
}