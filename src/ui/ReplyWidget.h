#pragma once

namespace discord
{
    class ReplyWidgetData;
}
namespace util
{
    class CancellationToken;
}
namespace ui
{
    class ReplyWidget : public Gtk::Widget
    {
    public:
        ReplyWidget(const SleepyDiscord::ReferencedMessage& message);
        ~ReplyWidget() override;

        void reset();

        const SleepyDiscord::ReferencedMessage& get_message() const { return m_message; }

    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;
        Gtk::SizeRequestMode get_request_mode_vfunc() const override;

        Gtk::Box* m_child = nullptr;
        Gtk::Picture* m_avatar = nullptr;

        util::CancellationToken* m_avatar_cancellation_token = nullptr;

        SleepyDiscord::ReferencedMessage m_message;
    };
}
