#pragma once

namespace ui
{
    class EmojiWidget : public Gtk::Widget
    {
    public:
        EmojiWidget();

        sigc::signal<void(const std::string& emoji)>& signal_emoji_selected() { return m_signal_emoji_selected; };
        sigc::signal<void(const SleepyDiscord::Emoji& emoji)>& signal_server_emoji_selected() { return m_signal_server_emoji_selected; };

    private:
        void load_emojis(Gtk::Box* box);

        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        Gtk::ScrolledWindow* m_child = nullptr;
        sigc::signal<void(const std::string& emoji)> m_signal_emoji_selected;
        sigc::signal<void(const SleepyDiscord::Emoji& emoji)> m_signal_server_emoji_selected;

        int m_load_index = 0;
        int m_sub_load_index = 0;
        Gtk::FlowBox* m_loading_flow_box = nullptr;
    };
}
