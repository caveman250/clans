#pragma once

namespace discord
{
    class MessageWidgetData;
}
namespace util
{
    class CancellationToken;
}
namespace ui
{
    class ReactionWidget;
    class FileWidget;
    class GifWidget;
class MessageWidget : public Gtk::Widget
    {
    public:
        MessageWidget();
        ~MessageWidget() override;

        void init(const Glib::RefPtr<discord::MessageWidgetData>& message);
        void reset();

        const SleepyDiscord::Message& get_message() const { return m_message; }

        void add_reaction(const SleepyDiscord::Reaction& reaction);
        void add_reaction(const SleepyDiscord::Emoji& reaction, bool me);
        void delete_reaction(const SleepyDiscord::Emoji& reaction, bool me);

    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;
        Gtk::SizeRequestMode get_request_mode_vfunc() const override;

        void setup(bool show_name, bool hide_text);
        void setup_attachment(const SleepyDiscord::Attachment& attachment);
        void setup_image_url(const Glib::RefPtr<discord::MessageWidgetData>& message);
        void setup_gif(const std::string& url);
        void setup_picture(const std::string& url, float aspect_ratio, int width, int height);
        void setup_reactions();

        Gtk::Box* m_child = nullptr;
        Gtk::Box* m_content_box = nullptr;
        Gtk::Box* m_vertical_box = nullptr;
        Gtk::Picture* m_avatar = nullptr;
        Gtk::FlowBox* m_reaction_box = nullptr;
        Gtk::Button* m_add_button = nullptr;
        std::vector<ReactionWidget*> m_reaction_widgets;

        util::CancellationToken* m_avatar_cancellation_token = nullptr;
        util::CancellationToken* m_image_cancellation_token = nullptr;
        Gtk::Picture* m_picture = nullptr;
        ui::GifWidget* m_gif = nullptr;

        SleepyDiscord::Message m_message;
    };
}
