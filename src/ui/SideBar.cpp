#include <util/ImageCache.h>
#include "SideBar.h"
#include "Application.h"
#include "discord/DiscordClient.h"
#include "util/ImageDownloader.h"
#include "ui/EmptyMessageView.h"
#include "UserWidget.h"

namespace ui
{
    void sidebar_on_application_flap_folded(AdwFlap*, GParamSpec* ,gpointer user_data)
    {
        auto header_bar = (GtkWidget*)user_data;
        auto flap = Application::Get()->get_flap();
        bool flap_folded = adw_flap_get_folded(flap);
        gtk_widget_set_visible(header_bar, !flap_folded);
    }

    SideBar::SideBar()
    {
        m_child = new Gtk::Box();
        m_child->set_orientation(Gtk::Orientation::VERTICAL);
        m_child->set_parent(*this);

        GtkWidget* header_bar = adw_header_bar_new();
        gtk_box_append(m_child->gobj(), header_bar);
        adw_header_bar_set_show_start_title_buttons((AdwHeaderBar*)header_bar, true);
        adw_header_bar_set_show_end_title_buttons((AdwHeaderBar*)header_bar, false);

        auto flap_button = new Gtk::Button();
        flap_button->set_icon_name("view-sidebar-start-symbolic");
        flap_button->signal_clicked().connect([this]()
        {
            auto flap = Application::Get()->get_flap();
            adw_flap_set_reveal_flap(flap, !adw_flap_get_reveal_flap(flap));
        });
        adw_header_bar_pack_start((AdwHeaderBar*)header_bar, (GtkWidget*)flap_button->gobj());

        m_scrolled_window = new Gtk::ScrolledWindow();
        m_scrolled_window->set_vexpand(true);
        m_scrolled_window->add_css_class("background");
        m_child->append(*m_scrolled_window);

        auto viewport = new Gtk::Viewport(Gtk::Adjustment::create(0,0,0), Gtk::Adjustment::create(0,0,0));
        m_scrolled_window->set_child(*viewport);

        m_mainbox = new Gtk::Box(Gtk::Orientation::VERTICAL);
        viewport->set_child(*m_mainbox);

        if (Application::Get()->get_discord_client())
        {
            m_spinner = new Gtk::Spinner();
            m_spinner->set_size_request(50, 50);
            m_spinner->set_valign(Gtk::Align::START);
            m_spinner->start();
            m_mainbox->append(*m_spinner);
        }

        m_listbox = new Gtk::ListBox();
        m_listbox->set_selection_mode(Gtk::SelectionMode::NONE);
        m_mainbox->append(*m_listbox);

        g_signal_connect(Application::Get()->get_flap(), "notify::folded", G_CALLBACK(sidebar_on_application_flap_folded), (gpointer)header_bar);

        if (Application::Get()->get_discord_client() && Application::Get()->get_discord_client()->is_ready())
        {
            on_discord_client_ready();
        }
    }

    SideBar::~SideBar()
    {
        for (const auto& pointer : m_channel_pointers)
        {
            delete pointer;
        }

        if (m_new_message_connection.connected())
        {
            m_new_message_connection.disconnect();
        }
    }

    void action_row_activated(AdwActionRow*, gpointer channel)
    {
        auto channel_ptr = (SleepyDiscord::Channel*)channel;
        if (channel_ptr->type == SleepyDiscord::Channel::SERVER_VOICE)
        {
            auto connected_channel = Application::Get()->get_discord_client()->connected_to_voice_channel();
            if (!connected_channel.empty())
            {
                Application::Get()->get_discord_client()->disconnect_from_voice_channel();
            }

            if (connected_channel.empty() || *channel_ptr != connected_channel)
            {
                Application::Get()->get_discord_client()->connect_to_voice_channel(*channel_ptr);
            }
        }
        else
        {
            Application::Get()->get_discord_client()->view_channel(*channel_ptr);
        }
    }

    void SideBar::on_discord_client_ready()
    {
        if (m_setup)
        {
            return;
        }

        if (m_spinner)
        {
            m_mainbox->remove(*m_spinner);
            m_spinner = nullptr;
        }

        m_listbox->set_vexpand(true);

        discord::DiscordClient* client = Application::Get()->get_discord_client();

        AdwExpanderRow* dm_item = create_dms_item();
        auto dm_channels = client->get_dm_channels();
        dm_channels.sort([client](const SleepyDiscord::Channel& left, const SleepyDiscord::Channel& right)
        {
            if (left.lastMessageID.empty() && right.lastMessageID.empty())
            {
                return false;
            }
            else if (left.lastMessageID.empty())
            {
                return false;
            }
            else if (right.lastMessageID.empty())
            {
                return true;
            }

            return left.lastMessageID.timestamp() > right.lastMessageID.timestamp();
        });
        for (const auto& channel : dm_channels)
        {
            create_text_channel_item(channel, dm_item);
        }

        for (const auto& server : client->get_servers())
        {
            auto server_new = client->getServer(server.ID);

            AdwExpanderRow* text_channels = (AdwExpanderRow*)adw_expander_row_new();
            AdwExpanderRow* voice_channels = (AdwExpanderRow*)adw_expander_row_new();
            create_server_item(server, text_channels, voice_channels);

            for (const auto& channel : client->get_server_channels(server))
            {
                switch (channel.type)
                {
                    case SleepyDiscord::Channel::ChannelType::GUILD_NEWS:
                    case SleepyDiscord::Channel::ChannelType::SERVER_TEXT:
                    {
                        if (client->has_permission(channel, SleepyDiscord::READ_MESSAGES | SleepyDiscord::VIEW_CHANNEL))
                        {
                            create_text_channel_item(channel, text_channels);
                        }
                        break;
                    }
                    case SleepyDiscord::Channel::ChannelType::SERVER_VOICE:
                    {
                        if (client->has_permission(channel, SleepyDiscord::VIEW_CHANNEL))
                        {
                            create_voice_channel_item(channel, voice_channels);
                        }
                        break;
                    }
                    default:
                        break;
                }
            }
        }

        m_new_message_connection = Application::Get()->get_discord_client()->signal_new_message().connect([this](const SleepyDiscord::Message& message)
        {
            if (Application::Get()->get_message_view()->get_channel().ID != message.channelID)
            {
                if (!message.content.empty() || !message.attachments.empty())
                {
                    mark_channel_unread(message.channelID);
                }
            }
        });

        update_user_voice_states();

        m_setup = true;
    }

    void SideBar::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void SideBar::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void SideBar::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    void SideBar::mark_channel_unread(const SleepyDiscord::Snowflake<SleepyDiscord::Channel>& channel)
    {
        int64_t channel_id = channel.number();
        if (m_channel_items.count(channel_id) > 0)
        {
            AdwActionRow* row = m_channel_items[channel_id];
            adw_action_row_set_icon_name(row, "mail-unread-symbolic");
        }
    }

    void SideBar::mark_channel_read(const SleepyDiscord::Snowflake<SleepyDiscord::Channel>& channel)
    {
        int64_t channel_id = channel.number();
        if (m_channel_items.count(channel_id) > 0)
        {
            AdwActionRow* row = m_channel_items[channel_id];
            adw_action_row_set_icon_name(row, "mail-read-symbolic");
        }
    }

    AdwExpanderRow* SideBar::create_dms_item()
    {
        GtkWidget* expander_row = adw_expander_row_new();
        adw_preferences_row_set_title((AdwPreferencesRow*)expander_row, "Direct Messages");
        gtk_list_box_append(m_listbox->gobj(), (GtkWidget*)expander_row);
        GtkWidget* avatar = adw_avatar_new(32, "Direct Messages", true);
        adw_expander_row_add_prefix((AdwExpanderRow*)expander_row, avatar);

        return (AdwExpanderRow*)expander_row;
    }

    void SideBar::create_server_item(const SleepyDiscord::Server& server, AdwExpanderRow* text_item, AdwExpanderRow* voice_item)
    {
        GtkWidget* expander_row = adw_expander_row_new();
        adw_preferences_row_set_title((AdwPreferencesRow*)expander_row, server.name.c_str());
        gtk_list_box_append(m_listbox->gobj(), (GtkWidget*)expander_row);
        GtkWidget* avatar = adw_avatar_new(32, server.name.c_str(), true);
        adw_expander_row_add_prefix((AdwExpanderRow*)expander_row, avatar);

        if (!server.icon.empty())
        {
            std::string url = util::Helpers::str_format("https://cdn.discordapp.com/icons/%s/%s.png?size=64", server.ID.string().c_str(), server.icon.c_str());
            util::ImageCache::get()->get_image(url, [avatar, server](const std::string& file_path)
            {
                try
                {
                    auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
                    adw_avatar_set_custom_image((AdwAvatar*) avatar, (GdkPaintable*) texture->gobj());
                }
                catch (std::exception& e)
                {
                    std::cout << e.what() << std::endl;
                }
            }, nullptr);
        }

        adw_preferences_row_set_title((AdwPreferencesRow*)text_item, "Text Channels");
        adw_expander_row_add_row((AdwExpanderRow*)expander_row, (GtkWidget*)text_item);
        adw_expander_row_set_expanded((AdwExpanderRow*)text_item, true);
        adw_expander_row_set_icon_name((AdwExpanderRow*)text_item, "format-justify-left-symbolic");

        adw_preferences_row_set_title((AdwPreferencesRow*)voice_item, "Voice Channels");
        adw_expander_row_add_row((AdwExpanderRow*)expander_row, (GtkWidget*)voice_item);
        adw_expander_row_set_expanded((AdwExpanderRow*)voice_item, true);
        adw_expander_row_set_icon_name((AdwExpanderRow*)voice_item, "audio-volume-high");
    }

    AdwActionRow* SideBar::create_text_channel_item(const SleepyDiscord::Channel& channel, AdwExpanderRow* parent)
    {
        GtkWidget* row = adw_action_row_new();

        std::string channel_name = channel.name;
        if (channel_name.empty())
        {
            for (unsigned int i = 0; i < channel.recipients.size(); ++i)
            {
                channel_name +=  channel.recipients[i].username;
                if (i < channel.recipients.size() - 1)
                {
                    channel_name += ", ";
                }
            }
        }

        size_t amp_it = channel_name.find('&');
        while (amp_it != std::string::npos)
        {
            channel_name.replace(amp_it, 1, "&amp;");
            amp_it = channel_name.find('&', amp_it + 1);
        }
        adw_preferences_row_set_title((AdwPreferencesRow*)row, channel_name.c_str());
        g_object_set(row, "activatable", true, (const char*)nullptr);

        m_channel_items[channel.ID.number()] = (AdwActionRow*)row;

        auto keep_alive = new SleepyDiscord::Channel(channel);
        m_channel_pointers.push_back(keep_alive);
        g_signal_connect(row, "activated", G_CALLBACK(action_row_activated), keep_alive);

        adw_expander_row_add_row(parent, (GtkWidget*) row);

        discord::DiscordClient* client = Application::Get()->get_discord_client();
        int64_t last_read = client->get_cache().get_last_seen_message_for_channel(channel);
        if (client->get_user_settings()->get_channel_muted(channel.serverID, channel))
        {
            gtk_widget_set_opacity((GtkWidget*)row, 0.5);
        }

        if (!channel.lastMessageID.empty() &&
            (last_read == -1 || last_read != channel.lastMessageID.number()))
        {
            mark_channel_unread(channel.ID);
        }
        else
        {
            mark_channel_read(channel.ID);
        }

        Gtk::Button* mute_button = new Gtk::Button();
        mute_button->set_icon_name(client->get_user_settings()->get_channel_muted(channel.serverID, channel) ? "audio-volume-muted-symbolic" : "audio-volume-high-symbolic");
        mute_button->set_valign(Gtk::Align::CENTER);
        mute_button->signal_clicked().connect([row, mute_button, client, channel]()
        {
            bool was_muted = client->get_user_settings()->get_channel_muted(channel.serverID, channel);
            client->get_user_settings()->set_channel_muted(channel.serverID, channel, !was_muted);
            mute_button->set_icon_name(was_muted ? "audio-volume-high-symbolic" : "audio-volume-muted-symbolic");
            gtk_widget_set_opacity((GtkWidget*)row, was_muted? 1.0 : 0.5);
        });
        adw_action_row_add_suffix((AdwActionRow*)row, (GtkWidget*)mute_button->gobj());

        return (AdwActionRow*)row;
    }

    Gtk::ListBoxRow* SideBar::create_voice_channel_item(const SleepyDiscord::Channel& channel, AdwExpanderRow* parent)
    {
        auto list_box_row = new Gtk::ListBoxRow();
        std::string channel_name = channel.name;
        if (channel_name.empty())
        {
            for (unsigned int i = 0; i < channel.recipients.size(); ++i)
            {
                channel_name +=  channel.recipients[i].username;
                if (i < channel.recipients.size() - 1)
                {
                    channel_name += ", ";
                }
            }
        }

        size_t amp_it = channel_name.find('&');
        while (amp_it != std::string::npos)
        {
            channel_name.replace(amp_it, 1, "&amp;");
            amp_it = channel_name.find('&', amp_it + 1);
        }

        auto child = new Gtk::Box(Gtk::Orientation::VERTICAL);
        list_box_row->set_child(*child);

        auto label = new Gtk::Label();
        label->set_text(channel_name);
        child->append(*label);
        list_box_row->set_activatable(true);

        auto keep_alive = new SleepyDiscord::Channel(channel);
        m_channel_pointers.push_back(keep_alive);

        auto gesture_click = Gtk::GestureClick::create();
        gesture_click->signal_released().connect([keep_alive](int n_press, double, double)
        {
            if (n_press <= 0)
            {
                return;
            }

            action_row_activated(nullptr, keep_alive);
        });
        list_box_row->add_controller(gesture_click);

        adw_expander_row_add_row(parent, (GtkWidget*) list_box_row->gobj());

        auto box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        child->append(*box);
        m_voice_channel_boxes[channel.ID.number()] = box;

        return list_box_row;
    }

    void SideBar::update_user_voice_states()
    {
        for (const auto& kvp : m_voice_channel_boxes)
        {
            for (auto user_widget = (UserWidget*)kvp.second->get_first_child(); user_widget != nullptr; user_widget = (UserWidget*)user_widget->get_next_sibling())
            {
                auto channel = Application::Get()->get_discord_client()->get_user_voice_channel(user_widget->get_user());
                if (channel.empty() || kvp.first != channel.number())
                {
                    kvp.second->remove(*user_widget);
                }
            }
        }

        for (const auto& voice_state : Application::Get()->get_discord_client()->get_voice_states())
        {
            if (!voice_state.second.empty())
            {
                auto box = m_voice_channel_boxes[voice_state.second.number()];
                bool found = false;
                for (auto user_widget = (UserWidget*)box->get_first_child(); user_widget != nullptr; user_widget = (UserWidget*)user_widget->get_next_sibling())
                {
                    if (user_widget->get_user() == voice_state.first)
                    {
                        found = true;
                        break;
                    }
                }

                if (!found)
                {
                    auto user_widget = new UserWidget(Application::Get()->get_discord_client()->getUser(voice_state.first));
                    m_voice_channel_boxes[voice_state.second.number()]->append(*user_widget);
                }
            }
        }
    }
}