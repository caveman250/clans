#pragma once

namespace ui
{
    class Popup;
    class ReactionWidget : public Gtk::Widget
    {
    public:
        ReactionWidget(const SleepyDiscord::Reaction& reaction, const SleepyDiscord::Snowflake<SleepyDiscord::Channel>& channel_id, const SleepyDiscord::Message& message);
        ~ReactionWidget();
        void set_users(const std::vector<SleepyDiscord::User>&  users);
        void update_count_label();

        const SleepyDiscord::Reaction& get_reaction() const { return m_reaction; }
        void set_reaction(const SleepyDiscord::Reaction& reaction);
        void update_colour();
    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;
        Gtk::SizeRequestMode get_request_mode_vfunc() const override;

        Gtk::Box* m_child;
        Gtk::Picture* m_picture = nullptr;
        Gtk::Label* m_count_label = nullptr;
        std::vector<SleepyDiscord::User> m_users;
        util::SimpleThread* m_fetch_users_thread = nullptr;
        Popup* m_popup = nullptr;

        SleepyDiscord::Reaction m_reaction;
        SleepyDiscord::Snowflake<SleepyDiscord::Channel> m_channel_id;
        SleepyDiscord::Message m_message;
        bool m_fetching_users = false;

        util::CancellationToken* m_image_cancellation_token = nullptr;
    };
}
