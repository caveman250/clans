#include "FileWidget.h"
#include "Application.h"
#include "util/Helpers.h"

namespace ui
{
    FileWidget::FileWidget(const SleepyDiscord::Attachment& file)
            : Gtk::Widget()
    {
        set_margin_top(5);
        set_margin_bottom(5);
        set_halign(Gtk::Align::START);

        m_child = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 10);
        m_child->set_parent(*this);
        m_child->add_css_class("card");

        auto image = new Gtk::Image();
        image->set_icon_size(Gtk::IconSize::LARGE);
        image->set_from_icon_name("text-x-generic-symbolic");
        image->set_margin_top(5);
        image->set_margin_bottom(5);
        image->set_margin_start(5);
        m_child->append(*image);

        auto center_box = new Gtk::Box(Gtk::Orientation::VERTICAL, 5);
        center_box->set_margin_top(5);
        center_box->set_margin_bottom(5);
        m_child->append(*center_box);

        auto download_label = new Gtk::Label();
        auto download_text = util::Helpers::str_format("<a href=\"%s\">%s</a>", file.url.c_str(), file.filename.c_str());
        download_label->set_markup(download_text);
        download_label->signal_activate_link().connect([file](const Glib::ustring& url)
        {
            util::Helpers::handle_link(file.url);
            return true;
        }, false);
        download_label->set_wrap(true);
        download_label->set_wrap_mode(Pango::WrapMode::WORD_CHAR);
        center_box->append(*download_label);

        auto size_label = new Gtk::Label();
        std::string file_size_text;
        if (file.size > 1000000) //1 mb
        {
            file_size_text = util::Helpers::str_format("%.2f MB", (float)file.size / 1000000.f);
        }
        else
        {
            file_size_text = util::Helpers::str_format("%.2f KB", (float)file.size / 1000.f);
        }
        size_label->set_markup(file_size_text);
        center_box->append(*size_label);

        auto button = new Gtk::Button();
        button->set_icon_name("folder-download-symbolic");
        button->set_valign(Gtk::Align::CENTER);
        button->set_margin_top(5);
        button->set_margin_bottom(5);
        button->set_margin_end(5);
        button->signal_clicked().connect([file]()
        {
            util::Helpers::handle_link(file.url);
        });
        m_child->append(*button);
    }

    FileWidget::~FileWidget()
    {

    }

    void FileWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void FileWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            m_child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void FileWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    Gtk::SizeRequestMode FileWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::HEIGHT_FOR_WIDTH;
    }
}