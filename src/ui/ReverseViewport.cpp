#include "ReverseViewport.h"

namespace ui
{
    void ReverseViewport::viewport_set_adjustment_values (Gtk::Orientation  orientation, int viewport_size, int child_size)
    {
        double upper, value;

        upper = child_size;

        auto adjustment = orientation == Gtk::Orientation::VERTICAL ? get_vadjustment() : get_hadjustment();
        value = adjustment->get_value();

        /* We clamp to the left in RTL mode */
        if (orientation == Gtk::Orientation::HORIZONTAL &&
            get_direction() == Gtk::TextDirection::RTL)
        {
            double dist =  adjustment->get_value()
                          - value
                          - adjustment->get_page_size();
            value = upper - dist - viewport_size;
        }

        if (orientation == Gtk::Orientation::VERTICAL)
        {
            int bottom_of_viewport = value + viewport_size;

            if (m_last_upper != upper || m_last_height != viewport_size)
            {
                if (m_distance_to_bottom > upper)
                {
                    m_distance_to_bottom = 0;
                }

                value = upper - m_distance_to_bottom - viewport_size;
                bottom_of_viewport = value + viewport_size;
                m_last_upper = upper;
                m_last_height = viewport_size;
            }
            else if (m_last_value != value)
            {
                m_signal_user_scrolled.emit(value);
            }

            m_distance_to_bottom = upper - bottom_of_viewport;
            m_last_value = value;
        }

        adjustment->configure(value, 0, upper, viewport_size * 0.1, viewport_size * 0.9, viewport_size);
        adjustment->signal_value_changed().connect([this]()
                                                          {
                                                              queue_allocate();
                                                          });
    }

    void ReverseViewport::compute_expand_vfunc(bool& hexpand_p, bool& vexpand_p)
    {
        if (m_child)
        {
            hexpand_p = m_child->compute_expand(Gtk::Orientation::HORIZONTAL);
            vexpand_p = m_child->compute_expand(Gtk::Orientation::VERTICAL);
        } else
        {
            hexpand_p = false;
            vexpand_p = false;
        }
    }

    ReverseViewport::ReverseViewport()
        : Glib::ObjectBase("ReverseViewport")
        , Gtk::Scrollable()
        , Gtk::Widget()
    {
        add_css_class("viewport");

        set_overflow(Gtk::Overflow::HIDDEN);

        set_vadjustment(Gtk::Adjustment::create(0, 0, 0));
        set_hadjustment(Gtk::Adjustment::create(0, 0, 0));

        get_vadjustment()->set_value(get_vadjustment()->get_upper());
        get_vadjustment()->signal_value_changed().connect([this]()
                                                          {
                                                              queue_allocate();
                                                          });

        queue_allocate();
    }

    ReverseViewport::~ReverseViewport()
    {
        m_child->unparent();
        m_child = nullptr;
    }

    void ReverseViewport::size_allocate_vfunc(int width, int height, int baseline)
    {
        int child_size[2];

        get_vadjustment()->freeze_notify();
        get_hadjustment()->freeze_notify();

        child_size[(int)Gtk::Orientation::HORIZONTAL] = width;
        child_size[(int)Gtk::Orientation::VERTICAL] = height;

        if (m_child && m_child->get_visible())
        {
            Gtk::Orientation orientation, opposite;
            int min, nat, min_baseline, nat_baseline;

            if (m_child->get_request_mode() == Gtk::SizeRequestMode::WIDTH_FOR_HEIGHT)
                orientation = Gtk::Orientation::VERTICAL;
            else
                orientation = Gtk::Orientation::HORIZONTAL;

            opposite = orientation == Gtk::Orientation::VERTICAL ? Gtk::Orientation::HORIZONTAL : Gtk::Orientation::VERTICAL;
            m_child->measure(orientation, -1, min, nat, min_baseline, nat_baseline);
            Gtk::Scrollable::Policy policy = orientation == Gtk::Orientation::VERTICAL ? get_vscroll_policy() : get_hscroll_policy();
            if (policy == Gtk::Scrollable::Policy::MINIMUM)
                child_size[(int)orientation] = MAX (child_size[(int)orientation], min);
            else
                child_size[(int)orientation] = MAX (child_size[(int)orientation], nat);

            m_child->measure(opposite, child_size[(int)orientation], min, nat, min_baseline, nat_baseline);
            Gtk::Scrollable::Policy opposite_policy = opposite == Gtk::Orientation::VERTICAL ? get_vscroll_policy() : get_hscroll_policy();
            if (opposite_policy == Gtk::Scrollable::Policy::MINIMUM)
                child_size[(int)opposite] = MAX (child_size[(int)opposite], min);
            else
                child_size[(int)opposite] = MAX (child_size[(int)opposite], nat);
        }

        viewport_set_adjustment_values(Gtk::Orientation::HORIZONTAL, width, child_size[GTK_ORIENTATION_HORIZONTAL]);
        viewport_set_adjustment_values(Gtk::Orientation::VERTICAL, height, child_size[GTK_ORIENTATION_VERTICAL]);

        if (m_child && m_child->get_visible())
        {
            Gtk::Allocation child_allocation;

            child_allocation.set_width(child_size[(int)Gtk::Orientation::HORIZONTAL]);
            child_allocation.set_height(child_size[(int)Gtk::Orientation::VERTICAL]);
            child_allocation.set_x(-get_hadjustment()->get_value());
            child_allocation.set_y(-get_vadjustment()->get_value());

            m_child->size_allocate(child_allocation, -1);
        }

        get_hadjustment()->thaw_notify();
        get_vadjustment()->thaw_notify();
    }

    void ReverseViewport::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            m_child->measure(orientation, for_size, minimum, natural, minimum_baseline, natural_baseline);
        }
    }

    void ReverseViewport::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    Gtk::SizeRequestMode ReverseViewport::get_request_mode_vfunc() const
    {
        if (m_child)
        {
            return m_child->get_request_mode();
        }
        else
        {
            return Gtk::SizeRequestMode::CONSTANT_SIZE;
        }
    }

    void ReverseViewport::set_child(Gtk::Widget* child)
    {
        m_child = child;
        m_child->set_parent(*this);
    }
}
