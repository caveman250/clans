#pragma once

namespace ui
{
    class Page : public Gtk::Widget
    {
    public:
        ~Page() override;

        virtual void create_header_bar() = 0;
        void update_flap_button_visibility();
    protected:
        void add_flap_button(bool only_when_not_folded);

        GtkHeaderBar* m_header_bar;
        std::vector<guint> m_signal_handlers;

        Gtk::Button* m_flap_button = nullptr;
        bool m_only_show_flap_button_when_not_folded = false;
    };
}

