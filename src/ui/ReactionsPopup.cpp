#include <util/Helpers.h>
#include <util/ImageCache.h>
#include <Application.h>
#include "ReactionsPopup.h"
#include "discord/DiscordClient.h"
#include "UserWidget.h"
#include "util/SimpleThread.h"

namespace ui
{
    ReactionsPopup::ReactionsPopup(const SleepyDiscord::Message& message, const SleepyDiscord::Reaction& initial_reaction, Gtk::Widget* parent, int override_x, int override_y)
            : Popup(parent, override_x, override_y)
    {
        auto notebook = new Gtk::Notebook();
        notebook->set_show_border(false);
        notebook->set_scrollable(true);

        int start_index = 0;
        for (const auto& reaction: message.reactions)
        {
            Gtk::Widget* tab_widget = nullptr;
            if (reaction.emoji.ID.empty())
            {
                auto label = new Gtk::Label();
                label->set_text(reaction.emoji.name);
                tab_widget = label;
            } else
            {
                auto picture = new Gtk::Picture();
                picture->set_size_request(8, 8);
                picture->set_valign(Gtk::Align::CENTER);
                picture->set_halign(Gtk::Align::CENTER);
                picture->set_can_shrink(true);
                picture->set_margin(4);
                tab_widget = picture;

                std::string url = util::Helpers::str_format("https://cdn.discordapp.com/emojis/%s.png", reaction.emoji.ID.string().c_str());
                util::ImageCache::get()->get_image(url, [picture](const std::string& file_path)
                {
                    try
                    {
                        auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
                        picture->set_paintable(texture);
                    }
                    catch (std::exception& e)
                    {
                        std::cout << e.what() << std::endl;
                    }
                }, nullptr);
            }

            auto scrolled_window = new Gtk::ScrolledWindow();
            scrolled_window->set_policy(Gtk::PolicyType::NEVER, Gtk::PolicyType::AUTOMATIC);

            auto viewport = new Gtk::Viewport(Gtk::Adjustment::create(0, 0, 1), Gtk::Adjustment::create(0, 0, 1));
            scrolled_window->set_child(*viewport);

            auto page = new Gtk::Box(Gtk::Orientation::VERTICAL, 10);
            page->set_vexpand(true);
            viewport->set_child(*page);

            std::string emoji_id;
            if (reaction.emoji.ID.empty())
            {
                emoji_id = reaction.emoji.name;
            } else
            {
                emoji_id = util::Helpers::str_format("%s:%s", reaction.emoji.name.c_str(), reaction.emoji.ID.string().c_str());
            }

            std::string initial_emoji_id;
            if (initial_reaction.emoji.ID.empty())
            {
                initial_emoji_id = initial_reaction.emoji.name;
            } else
            {
                initial_emoji_id = util::Helpers::str_format("%s:%s", initial_reaction.emoji.name.c_str(), initial_reaction.emoji.ID.string().c_str());
            }

            if (emoji_id == initial_emoji_id)
            {
                start_index = (int)m_page_data.size();
            }
            m_page_data.push_back(PageData { false, page, message, reaction, nullptr });
            notebook->append_page(*scrolled_window, *tab_widget);
        }

        notebook->signal_switch_page().connect([this](Gtk::Widget* widget, guint index)
                                               {
                                                   populate_users_for_page(index, m_page_data[index].page);
                                               });
        notebook->set_current_page(start_index);
        if (start_index == 0)
        {
            populate_users_for_page(0, m_page_data[0].page);
        }

        m_box->append(*notebook);
        m_box->set_size_request(-1, 400);

        open();
    }

    ReactionsPopup::~ReactionsPopup()
    {
        for (const auto& page_data : m_page_data)
        {
            if (page_data.thread)
            {
                page_data.thread->cancel();
            }
        }
    }

    void ReactionsPopup::populate_users_for_page(int index, Gtk::Box* page)
    {
        PageData& page_data = m_page_data[index];
        if (page_data.loaded || page_data.thread)
        {
            return;
        }

        auto spinner = new Gtk::Spinner();
        spinner->set_valign(Gtk::Align::START);
        spinner->set_size_request(64, 64);
        spinner->start();
        page->append(*spinner);

        page_data.thread = new util::SimpleThread([page_data](util::ThreadWorker* worker)
        {
            std::string emoji_id;
            if (page_data.reaction.emoji.ID.empty())
            {
                emoji_id = page_data.reaction.emoji.name;
            } else
            {
                emoji_id = util::Helpers::str_format("%s:%s", page_data.reaction.emoji.name.c_str(), page_data.reaction.emoji.ID.string().c_str());
            }
            try
            {
                auto temp = Application::Get()->get_discord_client()->getReactions(page_data.message.channelID, page_data.message.ID, emoji_id);
                auto* users = new std::vector<SleepyDiscord::User>(temp);
                return (void*) users;
            }
            catch (SleepyDiscord::ErrorCode code)
            {
                return (void*) nullptr;
            }
        },
        [&page_data, page, spinner](void* user_data, bool cancelled, util::SimpleThread*)
        {
            if (cancelled)
            {
                return;
            }

            spinner->unparent();
            delete spinner;

            auto* users = (std::vector<SleepyDiscord::User>*) user_data;
            if (users)
            {
                for (const auto& user: *users)
                {
                    auto user_widget = new UserWidget(user);
                    if (user == users->front())
                    {
                        user_widget->set_margin_top(10);
                    }
                    page->append(*user_widget);
                }
                page_data.loaded = true;
                page_data.thread = nullptr;
                delete users;
            }
        });
    }
}
