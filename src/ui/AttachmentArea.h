#pragma once

namespace ui
{
    class MessageBar;
    class AttachmentWidget;
    class AttachmentArea : public Gtk::Widget
    {
    public:
        AttachmentArea(MessageBar* parent);

        void set_reveal_child(bool reveal);
        bool get_reveal_child();

        std::vector<std::string> get_attachments();

        void add_attachment(const std::string file_path);
        void remove_attachment(AttachmentWidget* attachment);
    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;

        Gtk::Revealer* m_child = nullptr;
        Gtk::Box* m_box = nullptr;
        Gtk::Box* m_attachment_box = nullptr;

        std::vector<AttachmentWidget*> m_children;
    };
}
