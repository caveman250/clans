#include "AttachmentWidget.h"
#include "AttachmentArea.h"
#include "util/Helpers.h"
namespace ui
{
    AttachmentWidget::AttachmentWidget(AttachmentArea* parent)
    {
        m_child = new Gtk::Box();
        m_child->add_css_class("card");
        m_child->add_css_class("activatable");
        m_child->set_size_request(128, 128);
        m_child->set_parent(*this);

        auto clickHandler = Gtk::GestureClick::create();
        clickHandler->signal_released().connect([parent](int n_press, double, double)
        {
            auto fileChooser = Gtk::FileChooserNative::create("Choose file", Gtk::FileChooser::Action::OPEN, "Open", "Cancel");
            fileChooser->signal_response().connect([fileChooser, parent](int response)
            {
                if (response == Gtk::ResponseType::ACCEPT)
                {
                    parent->add_attachment(fileChooser->get_file()->get_path());
                    fileChooser->hide();
                }
            });
            fileChooser->show();
        });
        m_child->add_controller(clickHandler);

        auto image = new Gtk::Image(Gio::Icon::create("list-add-symbolic"));
        image->set_icon_size(Gtk::IconSize::LARGE);
        image->set_valign(Gtk::Align::CENTER);
        image->set_halign(Gtk::Align::CENTER);
        image->set_vexpand(true);
        image->set_hexpand(true);
        ((Gtk::Box*)m_child)->append(*image);
    }

    AttachmentWidget::AttachmentWidget(AttachmentArea* parent, const std::string& file_path)
    {
        m_filepath = file_path;

        m_child = new Gtk::Overlay();
        m_child->set_parent(*this);

        auto box = new Gtk::Box(Gtk::Orientation::VERTICAL);
        box->add_css_class("card");
        box->set_size_request(128, 128);
        ((Gtk::Overlay*)m_child)->set_child(*box);

        auto button = new Gtk::Button();
        button->set_icon_name("window-close-symbolic");
        button->set_halign(Gtk::Align::END);
        button->set_valign(Gtk::Align::START);
        button->signal_clicked().connect([this, parent]()
        {
            parent->remove_attachment(this);
        });
        ((Gtk::Overlay*)m_child)->add_overlay(*button);

        if (file_path.find(".gif") != std::string::npos ||
            file_path.find(".GIF") != std::string::npos ||
            file_path.find(".jpg") != std::string::npos ||
            file_path.find(".JPG") != std::string::npos ||
            file_path.find(".jpeg") != std::string::npos ||
            file_path.find(".JPEG") != std::string::npos ||
            file_path.find(".png") != std::string::npos ||
            file_path.find(".PNG") != std::string::npos)
        {
            m_picture = new Gtk::Picture();
            m_picture->set_margin(10);
            m_picture->set_halign(Gtk::Align::START);

            box->append(*m_picture);

            auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
            m_picture->set_paintable(texture);
        }
        else
        {
            auto image = new Gtk::Image();
            image->set_icon_size(Gtk::IconSize::LARGE);
            image->set_from_icon_name("text-x-generic-symbolic");
            image->set_margin_top(5);
            image->set_margin_bottom(5);
            image->set_margin_start(5);
            image->set_vexpand(true);
            image->set_hexpand(true);
            box->append(*image);

            std::string file_name = util::Helpers::get_filename(file_path);
            auto label = new Gtk::Label();
            label->set_text(file_name);
            label->set_margin_bottom(5);
            label->set_margin_start(5);
            label->set_margin_end(5);
            label->set_xalign(0.5);
            label->set_wrap(true);
            box->append(*label);
        }
    }

    void AttachmentWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void AttachmentWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void AttachmentWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }
}