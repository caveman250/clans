#pragma once

namespace ui
{
    class MessageWidget;
    class GifWidget : public Gtk::Widget
    {
    public:
        GifWidget(ui::MessageWidget* parent);
        virtual ~GifWidget();

        const Glib::RefPtr<Gdk::PixbufAnimation>& get_animation() const { return m_animation; }
        const Glib::RefPtr<Gdk::PixbufAnimationIter>& get_iter() const { return m_iter; }
        Gtk::Picture* get_picture() const { return m_child; }

        void set_gif(const std::string& file_path);
        void play(bool once);
        void pause() { m_paused = true; }

    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr <Gtk::Snapshot>& snapshot) override;

        ui::MessageWidget* m_parent_message = nullptr;

        Glib::RefPtr<Gdk::PixbufAnimation> m_animation;
        Glib::RefPtr<Gdk::PixbufAnimationIter> m_iter;
        Gtk::Picture* m_child;
        sigc::connection m_timeout_connection;
        sigc::connection m_scroll_connection;
        bool m_paused = true;
    };
}

