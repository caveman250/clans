#pragma once

namespace ui
{
    class ReverseViewport : public Gtk::Scrollable, public Gtk::Widget
    {
    public:
        ReverseViewport();
        ~ReverseViewport();

        void set_child(Gtk::Widget* child);
        Gtk::Widget* get_child() { return m_child; }
        void reset_offset() { m_distance_to_bottom = 0; }

        sigc::signal<void(double)> signal_user_scrolled() { return m_signal_user_scrolled; }
    private:
        void size_allocate_vfunc(int width, int height, int baseline) override;
        void measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const override;
        void snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot) override;
        Gtk::SizeRequestMode get_request_mode_vfunc() const override;
        void compute_expand_vfunc(bool& hexpand_p, bool& vexpand_p) override;

        void viewport_set_adjustment_values (Gtk::Orientation  orientation, int viewport_size, int child_size);

        Gtk::Widget* m_child = nullptr;

        sigc::signal<void(double)> m_signal_user_scrolled;
        double m_last_value = 0;

        int m_distance_to_bottom = 0;
        int m_last_upper = 0;
        int m_last_height = 0;
    };
}
