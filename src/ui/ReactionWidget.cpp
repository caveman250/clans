#include <util/SimpleThread.h>
#include <util/ImageCache.h>
#include "ReactionWidget.h"
#include "../util/Helpers.h"
#include "../Application.h"
#include "../discord/DiscordClient.h"
#include "util/CancellationToken.h"
#include "ReactionsPopup.h"

namespace ui
{
    ReactionWidget::ReactionWidget(const SleepyDiscord::Reaction& reaction, const SleepyDiscord::Snowflake<SleepyDiscord::Channel>& channel_id, const SleepyDiscord::Message& message)
     : Glib::ObjectBase("ReactionWidget")
     , Gtk::Widget()
     , m_reaction(reaction)
     , m_channel_id(channel_id)
     , m_message(message)
    {
        add_css_class("card");
        add_css_class("reaction");

        m_child = new Gtk::Box(Gtk::Orientation::HORIZONTAL, 5);
        m_child->set_parent(*this);
        m_child->set_margin(5);

        if (m_reaction.emoji.ID.empty())
        {
            auto label = new Gtk::Label();
            label->set_xalign(0);
            label->set_text(m_reaction.emoji.name);
            m_child->append(*label);
        }
        else
        {
            m_picture = new Gtk::Picture();
            m_picture->set_size_request(16, 16);
            m_picture->set_halign(Gtk::Align::START);

            m_child->append(*m_picture);

            std::string url = util::Helpers::str_format("https://cdn.discordapp.com/emojis/%s.png", reaction.emoji.ID.string().c_str());
            m_image_cancellation_token = new util::CancellationToken();
            util::ImageCache::get()->get_image(url, [this](const std::string& file_path)
            {
                try
                {
                    auto texture = Gdk::Texture::create_from_file(Gio::File::create_for_path(file_path));
                    m_picture->set_paintable(texture);
                }
                catch (std::exception& e)
                {
                    std::cout << e.what() << std::endl;
                }
                m_image_cancellation_token = nullptr;
            }, m_image_cancellation_token);
        }

        m_count_label = new Gtk::Label();
        update_count_label();
        m_child->append(*m_count_label);

        auto motion_controller = Gtk::EventControllerMotion::create();
        motion_controller->signal_motion().connect([this](double x, double y)
        {
            if (!m_fetching_users && m_users.empty())
            {
                m_fetching_users = true;
                m_fetch_users_thread = new util::SimpleThread([this](util::ThreadWorker* worker)
                {
                    std::string emoji_id;
                    if (m_reaction.emoji.ID.empty())
                    {
                        emoji_id = m_reaction.emoji.name;
                    } else
                    {
                        emoji_id = util::Helpers::str_format("%s:%s", m_reaction.emoji.name.c_str(), m_reaction.emoji.ID.string().c_str());
                    }

                    try
                    {
                        auto temp = Application::Get()->get_discord_client()->getReactions(m_channel_id, m_message.ID, emoji_id);
                        auto* users = new std::vector<SleepyDiscord::User>(temp);
                        return (void*) users;
                    }
                    catch (SleepyDiscord::ErrorCode code)
                    {
                        return (void*) nullptr;
                    }
                },
                [this](void* user_data, bool cancelled, util::SimpleThread*)
                {
                    auto* users = (std::vector<SleepyDiscord::User>*) user_data;
                    if (users)
                    {
                        if (!cancelled)
                        {
                            set_users(*users);
                        }
                        delete users;
                    }
                    if (!cancelled)
                    {
                        m_fetching_users = false;
                        m_fetch_users_thread = nullptr;
                    }
                });
            }
        });
        add_controller(motion_controller);

        auto long_press_controller = Gtk::GestureLongPress::create();
        long_press_controller->signal_pressed().connect([this](double, double)
        {
            m_popup = new ReactionsPopup(m_message, m_reaction, this);
            m_popup->signal_closed().connect([this]()
            {
                m_popup = nullptr;
            });
        });
        add_controller(long_press_controller);

        auto click_controller = Gtk::GestureClick::create();
        click_controller->signal_released().connect([this](int n_press, double, double)
        {
            if (n_press <= 0)
            {
                return;
            }

            if (m_popup)
            {
                return;
            }

            std::string emoji_id;
            if (m_reaction.emoji.ID.empty())
            {
                emoji_id = m_reaction.emoji.name;
            }
            else
            {
                emoji_id = util::Helpers::str_format("%s:%s", m_reaction.emoji.name.c_str(), m_reaction.emoji.ID.string().c_str());
            }

            if (m_reaction.me)
            {
                Application::Get()->get_discord_client()->removeReaction(m_message.channelID, m_message, emoji_id);
            }
            else
            {
                Application::Get()->get_discord_client()->addReaction(m_message.channelID, m_message, emoji_id);
            }
        });
        add_controller(click_controller);

        update_colour();
    }

    ReactionWidget::~ReactionWidget()
    {
        if (m_image_cancellation_token)
        {
            m_image_cancellation_token->cancel();
            m_image_cancellation_token = nullptr;
        }

        if (m_fetch_users_thread)
        {
            m_fetch_users_thread->cancel();
            m_fetch_users_thread = nullptr;
        }

        m_child->unparent();
        delete m_child;
    }

    void ReactionWidget::size_allocate_vfunc(int width, int height, int baseline)
    {
        Gdk::Rectangle rect = Gdk::Rectangle();
        rect.set_x(0);
        rect.set_y(0);
        rect.set_width(width);
        rect.set_height(height);

        if (m_child)
        {
            m_child->size_allocate(rect, baseline);
        }
    }

    void ReactionWidget::measure_vfunc(Gtk::Orientation orientation, int for_size, int& minimum, int& natural, int& minimum_baseline, int& natural_baseline) const
    {
        if (m_child)
        {
            int childMinimum, childNatural, childMinBaseLine, childNatBaseline;
            m_child->measure(orientation, for_size, childMinimum, childNatural, childMinBaseLine, childNatBaseline);
            natural = minimum = std::max(minimum, childMinimum);
        }
    }

    void ReactionWidget::snapshot_vfunc(const Glib::RefPtr<Gtk::Snapshot>& snapshot)
    {
        if (m_child)
        {
            snapshot_child(*m_child, snapshot);
        }
    }

    void ReactionWidget::set_users(const std::vector<SleepyDiscord::User>& users)
    {
        m_users = users;

        std::stringstream tooltip_ss;
        for (unsigned int i = 0; i < m_users.size(); ++i)
        {
            tooltip_ss << users[i].username;
            if (users.size() > 1 && i < users.size() - 2)
            {
                tooltip_ss << ", ";
            }
            else if (i < users.size() - 1)
            {
                tooltip_ss << " and ";
            }
        }

        tooltip_ss << " reacted with " << m_reaction.emoji.name;
        set_tooltip_text(tooltip_ss.str());
    }

    Gtk::SizeRequestMode ReactionWidget::get_request_mode_vfunc() const
    {
        return Gtk::SizeRequestMode::HEIGHT_FOR_WIDTH;
    }

    void ReactionWidget::update_count_label()
    {
        m_count_label->set_text(util::Helpers::str_format("%d", m_reaction.count));
    }

    void ReactionWidget::update_colour()
    {
        bool me = m_reaction.me;
        bool has_me_style_class = has_css_class("reaction-me");
        if (me && !has_me_style_class)
        {
            add_css_class("reaction-me");
        }
        else if (!me && has_me_style_class)
        {
            remove_css_class("reaction-me");
        }
    }

    void ReactionWidget::set_reaction(const SleepyDiscord::Reaction& reaction)
    {
        m_reaction = reaction;
        update_colour();
    }
}