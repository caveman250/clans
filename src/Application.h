#pragma once

namespace discord
{
    class DiscordClient;
}

namespace ui
{
    class Popup;
    class SideBar;
    class MessageView;
    class MessageBar;
    class EmptyMessageView;
}

class Application
{
public:
    static Application* Get();
    int run(int argc, char** argv);

    void login(const std::string& token);

    discord::DiscordClient* get_discord_client() const { return m_discord_client; }
    ui::EmptyMessageView* get_empty_message_view() const { return m_empty_message_view; }
    ui::MessageView* get_message_view() const { return m_message_view; }
    ui::MessageBar* get_message_bar() const { return m_message_bar; }
    ui::SideBar* get_side_bar() const { return m_sidebar; }

    int get_window_height() const;

    const Glib::RefPtr<Gtk::Application>& get_gtk_app() { return m_gtk_app; }
    AdwFlap* get_flap() const { return m_flap; }
    AdwLeaflet* get_leaflet() const { return m_leaflet; }
    Gtk::Revealer* get_popup_revealer() const { return m_popup_revealer; }
    Gtk::Box* get_popup_backer() const { return m_popup_backer; }
    void close_flap_if_needed() const;

    void on_discord_client_ready();
    void on_discord_client_login_failed();

    void set_title(const std::string& title);
    void activate_if_needed();
    void remove_empty_message_view_if_needed();

    void run_on_ui_thread(std::function<void()> fn);

    void navigate_backwards();
    void add_page(Gtk::Widget* page);
    void set_visible_page(Gtk::Widget* page);
    ui::Popup* get_showing_popup() { return m_displaying_popup; }
    void set_showing_popup(ui::Popup* popup);
    void increment_popup_background_persist_count();
    void decrement_popup_background_persist_count();
    void on_window_close();

private:
    void on_app_activate();
    void create_login_dialogue();

    void show_popup_background();
    void hide_popup_background();

    static Application* s_instance;

    GtkWindow* m_window = nullptr;
    Glib::RefPtr<Gtk::Application> m_gtk_app = nullptr;
    ui::SideBar* m_sidebar = nullptr;
    ui::EmptyMessageView* m_empty_message_view = nullptr;
    ui::MessageView* m_message_view = nullptr;
    ui::MessageBar* m_message_bar = nullptr;

    Gtk::Overlay* m_leaflet_overlay = nullptr;
    Gtk::Box* m_popup_backer = nullptr;
    Gtk::Revealer* m_popup_revealer = nullptr;
    AdwFlap* m_flap = nullptr;
    AdwLeaflet* m_leaflet = nullptr;

    Gtk::Spinner* m_login_spinner = nullptr;
    Gtk::Label* m_login_error = nullptr;
    Gtk::Dialog* m_login_dialogue = nullptr;
    Gtk::Button* m_login_button = nullptr;

    discord::DiscordClient* m_discord_client = nullptr;
    std::thread* m_discord_thread = nullptr;

    Glib::Dispatcher m_dispatcher;
    std::vector<std::function<void()>> m_dispatch_queue;
    std::mutex m_dispatch_mutex;

    ui::Popup* m_displaying_popup = nullptr;
    int m_popup_background_persist_count = 0;
};
