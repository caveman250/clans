#include "Application.h"
#include "util/HtmlMetaParser.h"


int main(int argc, char** argv)
{
    return Application::Get()->run(argc, argv);
}
