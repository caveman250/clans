#include "VoiceEventHandler.h"
#include "VoiceInput.h"

namespace discord
{
#define FORMAT PA_SAMPLE_S16LE
#define RATE 48000

    MyAudioOutput::MyAudioOutput()
            : SleepyDiscord::BaseAudioOutput()
    {
        m_ss.format = FORMAT;
        m_ss.channels = 2;
        m_ss.rate = RATE;

        m_s = pa_simple_new(NULL,               // Use the default server.
                          "Clans",           // Our application's name.
                          PA_STREAM_PLAYBACK,
                          NULL,               // Use the default device.
                          "Voice",            // Description of our stream.
                          &m_ss,                // Our sample format.
                          NULL,               // Use default channel map
                          NULL,               // Use default buffering attributes.
                          NULL               // Ignore error code.
        );

        m_mixer_thread = new std::thread([this]()
        {
            while (!m_shutting_down)
            {
                bool any_available = false;
                for (auto& user_stream : m_user_streams)
                {
                    any_available = !user_stream.second.empty();
                    if (any_available)
                    {
                        break;
                    }
                }

                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                {
                    m_mutex.lock();

                    size_t length = INT_MAX;
                    for (const auto& user_stream: m_user_streams)
                    {
                        if (!user_stream.second.empty())
                        {
                            length = std::min(length, user_stream.second.size());
                        }
                    }

                    if (length == 0 || length == INT_MAX)
                    {
                        m_mutex.unlock();
                        continue;
                    }

                    std::vector<std::vector<int16_t>> copy;
                    for (auto& user_stream: m_user_streams)
                    {
                        if (!user_stream.second.empty())
                        {
                            std::vector<int16_t> data(length);
                            memcpy(data.data(), user_stream.second.data(), length * 2);
                            copy.push_back(data);
                            user_stream.second.erase(user_stream.second.begin(), user_stream.second.begin() + length);
                        }
                    }

                    m_mutex.unlock();

                    std::vector<int16_t> mixed_audio(length, 0);
                    int mix_count = 0;
                    for (const auto& user_stream: copy)
                    {
                        for (unsigned int i = 0; i < user_stream.size(); ++i)
                        {
                            mixed_audio[i] += user_stream[i];
                        }

                        if (user_stream.size())
                        {
                            mix_count++;
                        }
                    }

                    pa_simple_write(m_s, reinterpret_cast<uint8_t*>(mixed_audio.data()), mixed_audio.size() * 2, nullptr);
                }
            }
        });
    }

    void MyAudioOutput::write(int16_t* pcm, uint length, uint32_t ssrc, uint16_t seq, SleepyDiscord::AudioTransmissionDetails& details)
    {
        if (!m_user_streams.count(ssrc))
        {
            m_user_streams[ssrc];
        }

        if (m_last_seq.count(ssrc) == 0)
        {
            m_last_seq[ssrc] = 0;
        }

        if (m_last_seq[ssrc] != 0 && seq != m_last_seq[ssrc])
        {
            if (seq > m_last_seq[ssrc] + 1)
            {
                int diff = seq - m_last_seq[ssrc] - 1;
                g_warning("missed %d packets for ssrc: %u", diff, ssrc);
            }
        }
        m_last_seq[ssrc] = seq;

        m_mutex.lock();
        std::vector<int16_t>& user_stream = m_user_streams[ssrc];
        size_t old_size = user_stream.size();
        user_stream.resize(old_size + length);
        memcpy(user_stream.data() + old_size, pcm, length * 2);
        m_mutex.unlock();
    }

    MyAudioOutput::~MyAudioOutput()
    {
        m_shutting_down = true;
        m_mixer_thread->join();
        delete m_mixer_thread;
    }

    void VoiceEventHandler::onReady(SleepyDiscord::VoiceConnection& connection)
    {
        connection.setAudioOutput(audioOut);
        connection.startSpeaking<VoiceInput>();
        connection.startListening();
    }

    VoiceEventHandler::~VoiceEventHandler()
    {
        g_warning("delete handler");
    }
}