#pragma once

namespace discord
{
    class ChannelStateCache
    {
    public:
        ChannelStateCache();
        void set_last_seen_message_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel, SleepyDiscord::Snowflake<SleepyDiscord::Message> message);
        void set_last_notified_time_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel, SleepyDiscord::Snowflake<SleepyDiscord::Message> message);
        int64_t get_last_seen_message_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel);
        int64_t get_last_notified_time_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel);
    private:
        void load();
        void save();

        std::unordered_map<int64_t, int64_t> m_last_notified_for_channel;
        std::unordered_map<int64_t, SleepyDiscord::Snowflake<SleepyDiscord::Message>> m_last_seen_for_channel;
    };
}
