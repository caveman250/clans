#include <stdio.h>
#include "pulse/pulseaudio.h"
#include "pulse/simple.h"

namespace discord
{
    struct MyAudioOutput : public SleepyDiscord::BaseAudioOutput
    {
    public:
        MyAudioOutput();
        ~MyAudioOutput() override;
        void write(int16_t* pcm, uint length, uint32_t ssrc, uint16_t seq, SleepyDiscord::AudioTransmissionDetails& details) override;
    private:
        bool m_shutting_down = false;

        pa_simple* m_s;
        pa_sample_spec m_ss;
        std::thread* m_mixer_thread;
        std::unordered_map<uint32_t, std::vector<int16_t>> m_user_streams;
        std::unordered_map<uint32_t, uint16_t> m_last_seq;
        std::mutex m_mutex;
    };

    class VoiceEventHandler : public SleepyDiscord::BaseVoiceEventHandler
    {
    public:
        VoiceEventHandler() = default;
        ~VoiceEventHandler() override;
        void onReady(SleepyDiscord::VoiceConnection& connection);
        SleepyDiscord::BaseAudioOutput* audioOut = new MyAudioOutput();
    };
}
