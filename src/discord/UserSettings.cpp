#include <Application.h>
#include "UserSettings.h"
#include "discord/DiscordClient.h"
#include "../util/Helpers.h"

namespace discord
{
    UserSettings::UserSettings(const std::list<SleepyDiscord::UserGuildSettings>& user_guild_settings)
    {
        for (const auto& guild_settings : user_guild_settings)
        {
            for (const auto& channel_override : guild_settings.channel_overrides)
            {
                m_muted_map[guild_settings.guild_id.number()][channel_override.channel_id.number()] = channel_override.muted;
            }
        }
    }

    bool UserSettings::get_channel_muted(SleepyDiscord::Snowflake<SleepyDiscord::Server> server_id, SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel)
    {
        if (server_id.empty())
        {
            return m_dm_muted_map[channel.number()];
        }
        if (m_muted_map.count(server_id.number()) != 0 && m_muted_map[server_id.number()].count(channel.number()) != 0)
        {
            return m_muted_map[server_id.number()][channel.number()];
        }

        return false;
    }

    void UserSettings::set_channel_muted(SleepyDiscord::Snowflake<SleepyDiscord::Server> server_id, SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel, bool muted)
    {
        if (server_id.empty())
        {
            m_dm_muted_map[channel.number()] = muted;
        }
        else
        {
            m_muted_map[server_id.number()][channel.number()] = muted;
            Application::Get()->get_discord_client()->muteChannel(server_id, channel, muted);
        }
    }
}