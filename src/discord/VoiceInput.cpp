#include "VoiceInput.h"

namespace discord
{
#define FORMAT PA_SAMPLE_S16LE
#define RATE 48000

    void input_context_state_cb(pa_context* context, void* mainloop) {
        pa_threaded_mainloop_signal((pa_threaded_mainloop*)mainloop, 0);
    }

    void input_stream_state_cb(pa_stream *s, void *mainloop) {
        pa_threaded_mainloop_signal((pa_threaded_mainloop*)mainloop, 0);
    }

    void input_stream_success_cb(pa_stream *stream, int success, void *userdata)
    {
        std::cout << "success" << std::endl;
    }

    void input_stream_read_cb(pa_stream *stream, size_t length, void *userdata)
    {
        const void* data;
        if (pa_stream_peek(stream, &data, &length) < 0)
        {
            return;
        }

        assert(data);
        assert(length > 0);

        auto voice_input = (VoiceInput*)userdata;
        auto& pending_audio = voice_input->get_pending_audio();

        size_t old_size = pending_audio.size();
        pending_audio.resize(old_size + length);
        memcpy(pending_audio.data() + old_size, data, length);

        pa_stream_drop(stream);
    }

    VoiceInput::VoiceInput()
            : SleepyDiscord::AudioVectorSource()
    {
// Get a mainloop and its context
        m_mainloop = pa_threaded_mainloop_new();
        assert(m_mainloop);
        m_mainloop_api = pa_threaded_mainloop_get_api(m_mainloop);
        m_context = pa_context_new(m_mainloop_api, "clans-mic-record");
        assert(m_context);

        // Set a callback so we can wait for the context to be ready
        pa_context_set_state_callback(m_context, &input_context_state_cb, m_mainloop);

        // Lock the mainloop so that it does not run and crash before the context is ready
        pa_threaded_mainloop_lock(m_mainloop);

        // Start the mainloop
        assert(pa_threaded_mainloop_start(m_mainloop) == 0);
        assert(pa_context_connect(m_context, NULL, PA_CONTEXT_NOAUTOSPAWN, NULL) == 0);

        // Wait for the context to be ready
        for(;;) {
            pa_context_state_t context_state = pa_context_get_state(m_context);
            assert(PA_CONTEXT_IS_GOOD(context_state));
            if (context_state == PA_CONTEXT_READY) break;
            pa_threaded_mainloop_wait(m_mainloop);
        }

        // Create a playback stream
        pa_sample_spec sample_specifications;
        sample_specifications.format = FORMAT;
        sample_specifications.rate = RATE;
        sample_specifications.channels = 2;

        pa_channel_map map;
        pa_channel_map_init_stereo(&map);

        m_stream = pa_stream_new(m_context, "clans-record", &sample_specifications, &map);
        pa_stream_set_state_callback(m_stream, input_stream_state_cb, m_mainloop);
        pa_stream_set_read_callback(m_stream, input_stream_read_cb, this);

        // recommended settings, i.e. server uses sensible values
        pa_buffer_attr buffer_attr;
        buffer_attr.fragsize = 24000;


        // Settings copied as per the chromium browser source
        pa_stream_flags_t stream_flags;
        stream_flags = (pa_stream_flags_t)(PA_STREAM_START_CORKED | PA_STREAM_INTERPOLATE_TIMING |
                                           PA_STREAM_NOT_MONOTONIC | PA_STREAM_AUTO_TIMING_UPDATE |
                                           PA_STREAM_ADJUST_LATENCY);

        assert(pa_stream_connect_record(m_stream, NULL, &buffer_attr, stream_flags) == 0);

        // Wait for the stream to be ready
        for(;;) {
            pa_stream_state_t stream_state = pa_stream_get_state(m_stream);
            assert(PA_STREAM_IS_GOOD(stream_state));
            if (stream_state == PA_STREAM_READY) break;
            pa_threaded_mainloop_wait(m_mainloop);
        }

        pa_threaded_mainloop_unlock(m_mainloop);

        // Uncork the stream so it will start playing
        pa_stream_cork(m_stream, 0, input_stream_success_cb, m_mainloop);
    }

    VoiceInput::~VoiceInput()
    {
        pa_threaded_mainloop_stop(m_mainloop);
        pa_stream_unref(m_stream);
        pa_context_disconnect(m_context);
        pa_context_unref(m_context);
        pa_threaded_mainloop_free(m_mainloop);
    }

    void VoiceInput::read(SleepyDiscord::AudioTransmissionDetails& details, std::vector<SleepyDiscord::AudioSample>& target)
    {
        if (!m_pending_audio.empty())
        {
            size_t size = std::min(target.size(), m_pending_audio.size() / 2);
            memcpy(target.data(), m_pending_audio.data(), size * sizeof(int16_t));
            m_pending_audio.erase(m_pending_audio.begin(), m_pending_audio.begin() + (size * 2));
        }
    }
}