#pragma once

#include "ui/MessageView.h"
#include "util/Helpers.h"
#include "ChannelStateCache.h"
#include "UserSettings.h"
#include "VoiceEventHandler.h"

namespace discord
{
    constexpr std::string_view s_user = "Ouchqt";

    class DiscordClient : public SleepyDiscord::DiscordClient
    {
    public:
        using SleepyDiscord::DiscordClient::DiscordClient;

        ~DiscordClient() override;

        bool is_ready() const;
        ChannelStateCache& get_cache() { return m_cache; }
        UserSettings* get_user_settings() { return m_user_settings; }
        std::list<SleepyDiscord::Server> get_servers();
        SleepyDiscord::Server get_server(SleepyDiscord::Snowflake<SleepyDiscord::Server> server_id);
        std::list<SleepyDiscord::Channel> get_server_channels(SleepyDiscord::Server server, bool force_refresh = false);
        std::list<SleepyDiscord::Channel> get_dm_channels(bool force_refresh = false);

        void view_channel(const SleepyDiscord::Channel& channel);

        void send_message(const std::string& message, std::vector<std::string> attachments = std::vector<std::string>());

        SleepyDiscord::Snowflake<SleepyDiscord::Channel> connected_to_voice_channel() { return m_connected_to_voice_channel; }
        bool is_connected_to_voice() { return !m_connected_to_voice_channel.empty(); }
        void connect_to_voice_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel);
        void disconnect_from_voice_channel();
        SleepyDiscord::Snowflake<SleepyDiscord::Channel> get_user_voice_channel(SleepyDiscord::Snowflake<SleepyDiscord::User> user) { return m_voice_states[user.number()]; }
        const std::map<int64_t, SleepyDiscord::Snowflake<SleepyDiscord::Channel>>& get_voice_states() { return m_voice_states; }

        // Signals
        sigc::signal<void(const SleepyDiscord::Message&)>& signal_new_message() { return m_signal_message; }

        bool has_permission(SleepyDiscord::Channel channel_id, SleepyDiscord::Permission permission);

        void check_for_missed_messages();
    private:

        void send_notification_for_channel(SleepyDiscord::Snowflake<SleepyDiscord::Channel> channel, SleepyDiscord::Message* message = nullptr);

        void cache_dm_channels(std::list<SleepyDiscord::Channel> channels);
        void cache_servers(std::list<SleepyDiscord::Server> servers);
        void cache_server_channels(SleepyDiscord::Server server, std::list<SleepyDiscord::Channel> channels);


        void onMessage(SleepyDiscord::Message message) override;
        virtual void onReaction(SleepyDiscord::Snowflake<SleepyDiscord::User> userID,
                                 SleepyDiscord::Snowflake<SleepyDiscord::Channel> channelID,
                                 SleepyDiscord::Snowflake<SleepyDiscord::Message> messageID,
                                 SleepyDiscord::Emoji emoji);
        virtual void onDeleteReaction(SleepyDiscord::Snowflake<SleepyDiscord::User> userID,
                                SleepyDiscord::Snowflake<SleepyDiscord::Channel> channelID,
                                SleepyDiscord::Snowflake<SleepyDiscord::Message> messageID,
                                SleepyDiscord::Emoji emoji);
        void onReady(SleepyDiscord::Ready readyData) override;
        void onResumed() override;
        void onError(SleepyDiscord::ErrorCode errorCode, const std::string errorMessage) override;
        void onEditVoiceState(SleepyDiscord::VoiceState& state) override;
        void onServer(SleepyDiscord::Server server) override;

        bool m_is_ready = false;

        SleepyDiscord::User m_current_user;
        std::list<SleepyDiscord::Server> m_servers;
        std::map<int64_t, std::list<SleepyDiscord::Channel>> m_channels;
        std::map<int64_t, SleepyDiscord::Snowflake<SleepyDiscord::Channel>> m_voice_states;
        std::list<SleepyDiscord::Channel> m_dm_channels;

        VoiceEventHandler* m_voice_event_handler = nullptr;

        ChannelStateCache m_cache;
        UserSettings* m_user_settings = nullptr;
        SleepyDiscord::Snowflake<SleepyDiscord::Channel> m_connected_to_voice_channel;

        // Signals
        sigc::signal<void(const SleepyDiscord::Message&)> m_signal_message;
    };
}
