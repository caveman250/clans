#include "Application.h"
#include "discord/DiscordClient.h"
#include "ui/MessageBar.h"
#include "ui/SideBar.h"
#include "ui/Popup.h"
#include <filesystem>
#include <ui/EmptyMessageView.h>
#include "util/SimpleThread.h"
#include "util/PasswordManager.h"

Application* Application::s_instance = nullptr;

int Application::run(int argc, char** argv)
{
    adw_init();

    std::string token = util::PasswordManager::load_token();
    if (!token.empty())
    {
        login(token);
    }

    auto application = Gtk::Application::create("io.gitlab.caveman250.clans");
    m_gtk_app = application;
    application->signal_activate().connect([this](){ on_app_activate(); });
    int ret = application->run(argc, argv);

    if (m_discord_thread)
    {
        m_discord_client->quit();
        m_discord_thread->join();
    }

    return ret;
}

void Application::login(const std::string& token)
{
    util::PasswordManager::save_token(token);
    m_discord_client = new discord::DiscordClient(token, SleepyDiscord::USER_CONTROLED_THREADS);
    SleepyDiscord::IntentsRaw intents =
            SleepyDiscord::Intent::SERVERS |
            SleepyDiscord::Intent::SERVER_MEMBERS |
            SleepyDiscord::Intent::SERVER_BANS |
            SleepyDiscord::Intent::SERVER_EMOJIS |
            SleepyDiscord::Intent::SERVER_INTEGRATIONS |
            SleepyDiscord::Intent::SERVER_WEBHOOKS |
            SleepyDiscord::Intent::SERVER_INVITES |
            SleepyDiscord::Intent::SERVER_VOICE_STATES |
            SleepyDiscord::Intent::SERVER_PRESENCES |
            SleepyDiscord::Intent::SERVER_MESSAGES |
            SleepyDiscord::Intent::SERVER_MESSAGE_REACTIONS |
            SleepyDiscord::Intent::SERVER_MESSAGE_TYPING |
            SleepyDiscord::Intent::DIRECT_MESSAGES |
            SleepyDiscord::Intent::DIRECT_MESSAGE_REACTIONS |
            SleepyDiscord::Intent::DIRECT_MESSAGE_TYPING;

    m_discord_client->setIntents(intents);
    m_discord_thread = new std::thread([this]()
    {
        m_discord_client->run();
    });
}

Application* Application::Get()
{
    if (!s_instance)
    {
        s_instance = new Application();
    }

    return s_instance;
}

void Application::on_app_activate()
{
    m_dispatcher.connect([this]()
    {
        m_dispatch_mutex.lock();
        for (const auto& action : m_dispatch_queue)
        {
            action();
        }
        m_dispatch_queue.clear();
        m_dispatch_mutex.unlock();
    });

    if (m_window)
    {
        gtk_widget_show((GtkWidget*)m_window);
        gtk_widget_grab_focus((GtkWidget*)m_window);
        return;
    }

    //ensure directories exist
    std::string config_dir = util::Helpers::str_format("%s/clans", Glib::get_user_config_dir().c_str());
    std::string cache_dir = util::Helpers::str_format("%s/clans", Glib::get_user_cache_dir().c_str());

    if (!std::filesystem::exists(config_dir))
    {
        std::filesystem::create_directory(config_dir);
    }
    if (!std::filesystem::exists(cache_dir))
    {
        std::filesystem::create_directory(cache_dir);
    }

    auto css_provider = Gtk::CssProvider::create();
    css_provider->load_from_resource("/io/gitlab/caveman250/clans/style.css");
    Gtk::StyleContext::add_provider_for_display(Gdk::Display::get_default(), css_provider, GTK_STYLE_PROVIDER_PRIORITY_APPLICATION);

    m_window = (GtkWindow*)adw_window_new();
    gtk_window_set_title(m_window, "Clans");
    gtk_window_set_default_size(m_window, 1280, 720);

    m_flap = (AdwFlap*)adw_flap_new();
    Gtk::Separator* seperator = new Gtk::Separator();
    seperator->set_orientation(Gtk::Orientation::VERTICAL);
    adw_flap_set_separator(m_flap, (GtkWidget*)seperator->gobj());

    m_leaflet_overlay = new Gtk::Overlay();
    m_leaflet = (AdwLeaflet*)adw_leaflet_new();
    gtk_widget_set_hexpand((GtkWidget*)m_leaflet, true);
    gtk_overlay_set_child(m_leaflet_overlay->gobj(), (GtkWidget*)m_leaflet);

    m_popup_backer = new Gtk::Box();
    m_popup_backer->set_vexpand(true);
    m_popup_backer->add_css_class("popup_dimming");

    auto gesture_click = Gtk::GestureClick::create();
    gesture_click->signal_released().connect([this](unsigned int n_press, double, double)
 {
     if (n_press > 0)
     {
         if (m_displaying_popup)
         {
             m_displaying_popup->close(false);
             m_displaying_popup = nullptr;
         }
     }
 });

    m_popup_backer->add_controller(gesture_click);
    m_popup_backer->set_visible(false);
    m_leaflet_overlay->add_overlay(*m_popup_backer);

    m_popup_revealer = new Gtk::Revealer();
    m_leaflet_overlay->add_overlay(*m_popup_revealer);
    m_popup_revealer->set_valign(Gtk::Align::END);

    m_sidebar = new ui::SideBar();
    m_sidebar->set_size_request(300, -1);
    m_sidebar->set_vexpand(true);

    adw_flap_set_flap(m_flap, (GtkWidget*)m_sidebar->gobj());
    adw_flap_set_content(m_flap, (GtkWidget*)m_leaflet_overlay->gobj());
    adw_window_set_content((AdwWindow*)m_window, (GtkWidget*)m_flap);

    m_empty_message_view = new ui::EmptyMessageView();
    m_empty_message_view->create_header_bar();
    adw_leaflet_append(m_leaflet, m_empty_message_view->gobj());
    adw_leaflet_set_can_unfold(m_leaflet, false);
    adw_leaflet_set_visible_child(m_leaflet, m_empty_message_view->gobj());

    auto main_box = new Gtk::Box(Gtk::Orientation::VERTICAL, 0);
    main_box->set_hexpand(true);
    main_box->set_vexpand(true);
    adw_leaflet_append(m_leaflet, (GtkWidget*)main_box->gobj());

    m_message_view = new ui::MessageView();
    m_message_view->create_header_bar();
    m_message_view->set_vexpand(true);
    m_message_view->set_size_request(360, -1);
    main_box->append(*m_message_view);

    m_message_bar = new ui::MessageBar();
    m_message_bar->signal_send().connect([this](const std::string& text, std::vector<std::string> attachments, SleepyDiscord::Message replying_to)
    {
        new util::SimpleThread([this, text, attachments, replying_to](util::ThreadWorker*) mutable
        {
            if (!replying_to.empty())
            {
                if (replying_to.serverID.empty())
                {
                    replying_to.serverID = m_message_view->get_channel().serverID;
                }
                replying_to.reply(m_discord_client, text);
            }
            else
            {
                m_discord_client->send_message(text, attachments);
            }
            return nullptr;
        }, nullptr);
    });
    main_box->append(*m_message_bar);

    gtk_application_add_window(m_gtk_app->gobj(), m_window);
    gtk_widget_show((GtkWidget*)m_window);

    if (!m_discord_thread)
    {
        create_login_dialogue();
    }
}

void Application::on_window_close()
{
    m_gtk_app->hold();
    gtk_application_remove_window(m_gtk_app->gobj(), m_window);
    gtk_window_destroy(m_window);
    m_window = nullptr;
}

bool window_close_request(gpointer)
{
    Application::Get()->on_window_close();
    return true;
}

void Application::on_discord_client_ready()
{
    if (m_login_dialogue)
    {
        m_login_dialogue->hide();
    }

    g_signal_connect(m_window, "close-request", G_CALLBACK(window_close_request), nullptr);

    m_sidebar->on_discord_client_ready();
    m_message_view->on_discord_client_ready();
}

void Application::on_discord_client_login_failed()
{
    if (!m_login_dialogue)
    {
        create_login_dialogue();
    }

    m_login_button->set_sensitive(true);
    m_login_button->set_label("Login");

    if (m_login_spinner)
    {
        m_login_spinner->unparent();
        delete m_login_spinner;
        m_login_spinner = nullptr;
    }

    m_login_error->set_text("Authentication Failed.");
    m_login_error->set_visible(true);
}

void Application::close_flap_if_needed() const
{
    if (adw_flap_get_folded(m_flap))
    {
        adw_flap_set_reveal_flap(m_flap, false);
    }
}

void Application::set_title(const std::string& title)
{
    gtk_window_set_title(m_window, title.c_str());
}

void Application::activate_if_needed()
{
    on_app_activate();
}

void Application::remove_empty_message_view_if_needed()
{
    if (m_empty_message_view)
    {
        adw_leaflet_remove(m_leaflet, (GtkWidget*)m_empty_message_view->gobj());
        m_empty_message_view->unparent();
        delete m_empty_message_view;
        m_empty_message_view = nullptr;
    }
}

void Application::run_on_ui_thread(std::function<void()> fn)
{
    m_dispatch_mutex.lock();
    m_dispatch_queue.push_back(fn);
    m_dispatcher.emit();
    m_dispatch_mutex.unlock();
}

void Application::create_login_dialogue()
{
    m_login_dialogue = new Gtk::Dialog("Log In", true, true);
    m_login_dialogue->set_size_request(360, 480);

    m_login_button = new Gtk::Button();
    m_login_button->set_label("Log In");
    m_login_button->add_css_class("suggested-action");
    m_login_button->set_sensitive(false);
    m_login_dialogue->add_action_widget(*m_login_button, Gtk::ResponseType::ACCEPT);
    m_login_dialogue->add_button("Cancel", Gtk::ResponseType::CANCEL);

    Gtk::Box* content_box = new Gtk::Box(Gtk::Orientation::VERTICAL, 10);
    content_box->set_halign(Gtk::Align::FILL);
    content_box->set_valign(Gtk::Align::CENTER);
    content_box->set_vexpand(true);

    m_login_error = new Gtk::Label();
    m_login_error->add_css_class("error_text");
    content_box->append(*m_login_error);
    m_login_error->set_visible(false);
    m_login_error->set_valign(Gtk::Align::END);
    m_login_error->set_halign(Gtk::Align::CENTER);

    auto label = new Gtk::Label("Enter your Discord token.");
    label->set_valign(Gtk::Align::END);
    label->set_halign(Gtk::Align::CENTER);
    content_box->append(*label);

    Gtk::Entry* token_entry = new Gtk::Entry();
    token_entry->set_valign(Gtk::Align::FILL);
    token_entry->set_margin_start(20);
    token_entry->set_margin_end(20);
    token_entry->signal_changed().connect([this, token_entry]()
    {
        m_login_button->set_sensitive(token_entry->get_text_length() > 0);
    });
    content_box->append(*token_entry);
    m_login_dialogue->get_content_area()->append(*content_box);
    m_login_dialogue->signal_response().connect([this, token_entry](int response)
    {
        if (response == Gtk::ResponseType::ACCEPT)
        {
            m_login_button->set_sensitive(false);
            m_login_button->set_label("");
            m_login_spinner = new Gtk::Spinner();
            m_login_spinner->start();
            m_login_button->set_child(*m_login_spinner);
            login(token_entry->get_text());
        } else
        {
            gtk_window_close(m_window);
        }
    });
    m_login_dialogue->show();
}

void Application::navigate_backwards()
{
    GtkWidget* previous_child = adw_leaflet_get_adjacent_child(m_leaflet, ADW_NAVIGATION_DIRECTION_BACK);
    adw_leaflet_set_visible_child(m_leaflet, previous_child);
}

void Application::add_page(Gtk::Widget* widget)
{
    while (GtkWidget* next_child = adw_leaflet_get_adjacent_child(m_leaflet, ADW_NAVIGATION_DIRECTION_FORWARD))
    {
        adw_leaflet_remove(m_leaflet, next_child);
    }

    adw_leaflet_append(m_leaflet, widget->gobj());
    adw_leaflet_set_visible_child(m_leaflet, widget->gobj());
}

void Application::set_visible_page(Gtk::Widget* page)
{
    adw_leaflet_set_visible_child(m_leaflet, page->gobj());
}

int Application::get_window_height() const
{
    GtkAllocation* allocation = new GtkAllocation();
    gtk_widget_get_allocation((GtkWidget*)m_window, allocation);
    int height = allocation->height;
    delete allocation;
    return height;

}

static void animation_cb (double value,
              Application* self)
{
    self->get_popup_backer()->set_opacity(value);
}

void Application::set_showing_popup(ui::Popup* popup)
{
    if (m_displaying_popup && popup)
    {
        g_warning("Attempting to display a popup while another popup is open. aborting.");
        return;
    }

    if (popup)
    {
        increment_popup_background_persist_count();
    }
    else if (m_displaying_popup && !popup)
    {
        decrement_popup_background_persist_count();
    }

    m_displaying_popup = popup;
}

void Application::increment_popup_background_persist_count()
{
    if (m_popup_background_persist_count == 0)
    {
        show_popup_background();
    }
    m_popup_background_persist_count++;
}

void Application::decrement_popup_background_persist_count()
{
    m_popup_background_persist_count--;
    if (m_popup_background_persist_count <= 0)
    {
        hide_popup_background();
    }
}

void Application::show_popup_background()
{
    AdwAnimationTarget* target = adw_callback_animation_target_new((AdwAnimationTargetFunc) animation_cb, this, NULL);
    g_autoptr(AdwAnimation)animation = adw_timed_animation_new ((GtkWidget*)m_popup_backer->gobj(), 0, 1, 250, target);

    adw_animation_play (animation);
}

void Application::hide_popup_background()
{
    AdwAnimationTarget* target = adw_callback_animation_target_new((AdwAnimationTargetFunc) animation_cb, this, NULL);
    g_autoptr(AdwAnimation)animation = adw_timed_animation_new ((GtkWidget*)m_popup_backer->gobj(), 1, 0, 250, target);

    adw_animation_play (animation);
}

